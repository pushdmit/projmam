<?php

use yii\db\Migration;

class m160607_071830_task_trackers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%task_trackers}}', [
            'id' => $this->primaryKey(),
			'project_id' => $this->integer()->notNull()->comment('Проект'),
	        'name' => $this->string()->notNull()->comment('Название'),
			'sort' => $this->integer()->notNull()->defaultValue(0)->comment('Позиция'),
        ], $tableOptions);

		$columns = ['id', 'project_id', 'name', 'sort'];
		$this->batchInsert('{{%task_trackers}}', $columns, [
			[1, 1, 'Не определен', 0],
		]);

		$this->addColumn(
			'{{%tasks}}', 'task_trackers_id',
			$this->integer()->notNull()->defaultValue(1)->after('user_id')->comment('Трекер')
		);

        $this->addForeignKey('{{%task_trackers_fk1}}', '{{%task_trackers}}', 'project_id', '{{%projects}}', 'id');
        $this->addForeignKey('{{%tasks_ibfk_1}}', '{{%tasks}}', 'task_trackers_id', '{{%task_trackers}}', 'id');
	}

    public function down()
    {
		$this->dropForeignKey('{{%tasks_ibfk_1}}', '{{%tasks}}');
		$this->dropColumn('{{%tasks}}', 'task_trackers_id');
        $this->dropTable('{{%task_trackers}}');
    }
}
