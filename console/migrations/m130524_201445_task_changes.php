<?php

use yii\db\Migration;

class m130524_201445_task_changes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%task_changes}}', [
	        'id' => $this->primaryKey(),
	        'task_id' => $this->integer()->notNull(),
	        'user_id' => $this->integer()->notNull(),
            'fields' => $this->text()->notNull(),
	        'date_create' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%task_change_status}}', [
	        'task_id' => $this->integer()->notNull(),
	        'user_id' => $this->integer()->notNull(),
	        'task_status_id_old' => $this->integer()->notNull(),
	        'task_status_id_new' => $this->integer()->notNull(),
	        'date_create' => $this->timestamp(),
        ], $tableOptions);

        $this->addForeignKey('{{%task_changes_task_id}}', '{{%task_changes}}', 'task_id', '{{%tasks}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('{{%task_changes_user_id}}', '{{%task_changes}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('{{%task_change_status_task_id}}', '{{%task_change_status}}', 'task_id', '{{%tasks}}', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('{{%task_change_status_user_id}}', '{{%task_change_status}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%task_changes}}');
        $this->dropTable('{{%task_change_status}}');
    }
}
