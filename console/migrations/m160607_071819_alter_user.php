<?php

use yii\db\Migration;

class m160607_071819_alter_user extends Migration
{
	public function safeUp()
	{
		$this->addColumn('{{%user}}', 'last_name', $this->string(32)->after('username')->comment('Фамилия'));
		$this->addColumn('{{%user}}', 'first_name', $this->string(32)->after('last_name')->comment('Имя'));
		$this->addColumn('{{%user}}', 'middle_name', $this->string(32)->after('first_name')->comment('Фамилия'));
	}

	public function safeDown()
	{
		$this->dropColumn('{{%user}}', 'first_name');
		$this->dropColumn('{{%user}}', 'last_name');
		$this->dropColumn('{{%user}}', 'middle_name');
	}
}
