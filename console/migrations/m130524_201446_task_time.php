<?php

use yii\db\Migration;

class m130524_201446_task_time extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%task_time}}', [
	        'id' => $this->primaryKey(),
	        'task_id' => $this->integer()->notNull(),
	        'user_id' => $this->integer()->notNull(),
            'quantity' => $this->decimal(9,2)->notNull(),
            'comment' => $this->text(),
	        'date_create' => $this->timestamp(),
        ], $tableOptions);

        $this->addForeignKey('{{%task_time_task_id}}', '{{%task_time}}', 'task_id', '{{%tasks}}', 'id');
        $this->addForeignKey('{{%task_time_user_id}}', '{{%task_time}}', 'user_id', '{{%user}}', 'id');

	    $this->addColumn('{{%tasks}}', 'time_evaluation', $this->decimal(9,2)->defaultValue(0));
	    $this->addColumn('{{%tasks}}', 'time_elapsed', $this->decimal(9,2)->defaultValue(0));
    }

    public function down()
    {
	    $this->dropColumn('{{%tasks}}', 'time_elapsed');
	    $this->dropColumn('{{%tasks}}', 'time_evaluation');
        $this->dropTable('{{%task_time}}');
    }
}
