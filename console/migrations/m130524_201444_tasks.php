<?php

use yii\db\Migration;

class m130524_201444_tasks extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tasks}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
	        'project_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'user_executor_id' => $this->integer()->notNull(),
            'task_status_id' => $this->integer()->notNull()->defaultValue(10),
	        'priority' => $this->smallInteger()->notNull()->defaultValue(5),
	        'readiness' => $this->smallInteger()->notNull()->defaultValue(0),
	        'parent_task_id' => $this->integer(),
	        'date_update' => $this->timestamp(),
	        'date_create' => $this->timestamp(),
	        'date_start' => $this->date(),
	        'date_end' => $this->date(),
            'is_delete' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('{{%task_status}}', [
            'id' => $this->primaryKey(),
	        'name' => $this->string()->notNull(),
            'project_id' => $this->integer()->notNull(),
	        'is_closed' => $this->integer()->defaultValue(0),
	        'is_active' => $this->integer()->defaultValue(1),
        ], $tableOptions);

        $this->addForeignKey('{{%tasks_project_id}}', '{{%tasks}}', 'project_id', '{{%projects}}', 'id');
        $this->addForeignKey('{{%tasks_user_id}}', '{{%tasks}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('{{%tasks_user_executor_id}}', '{{%tasks}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('{{%tasks_task_status_id}}', '{{%tasks}}', 'task_status_id', '{{%task_status}}', 'id');

        $this->addForeignKey('{{%task_status_project_id}}', '{{%task_status}}', 'project_id', '{{%projects}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%tasks}}');
        $this->dropTable('{{%task_status}}');
    }
}
