<?php

use yii\db\Migration;

class m160607_071821_case extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%task_case}}', [
            'id' => $this->primaryKey(),
	        'user_id' => $this->integer()->notNull(),
	        'task_id' => $this->integer()->notNull(),
	        'number' => $this->smallInteger()->notNull(),
	        'is_closed' => $this->smallInteger()->defaultValue(0),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('{{%task_case_user_id}}', '{{%task_case}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('{{%task_case_task_id}}', '{{%task_case}}', 'task_id', '{{%tasks}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%task_case}}');
    }
}
