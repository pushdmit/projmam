<?php

use yii\db\Migration;

class m130524_201443_projects extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'is_active' => 'CHAR(1) DEFAULT \'N\'',
            'is_public' => 'CHAR(1) DEFAULT \'N\'',
        ], $tableOptions);

        $this->createTable('{{%project_users}}', [
	        'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('{{%projects_user_id}}', '{{%projects}}', 'user_id', '{{%user}}', 'id');

        $this->addForeignKey('{{%project_project_id}}', '{{%project_users}}', 'project_id', '{{%projects}}', 'id');
        $this->addForeignKey('{{%project_users_user_id}}', '{{%project_users}}', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%project_users}}');
        $this->dropTable('{{%projects}}');
    }
}
