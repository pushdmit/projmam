<?php

use yii\db\Migration;

class m160607_071822_finance extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%finance}}', [
            'id' => $this->primaryKey(),
	        'name' => $this->string()->notNull()->comment('Название'),
            'user_id' => $this->integer()->notNull()->comment('Создатель'),
        ], $tableOptions);

	    //Виды статей расходов
	    $this->createTable('{{%finance_category}}', [
		    'id' => $this->primaryKey(),
		    'finance_id' => $this->integer()->notNull()->comment('Компания'),
		    'name' => $this->string()->notNull()->comment('Название (операцоиная, инвестиционная, Финансовая)'),
		    'description' => $this->text(),
	    ], $tableOptions);

	    //Статья расходов
        $this->createTable('{{%finance_expenditure}}', [
            'id' => $this->primaryKey(),

	        'finance_id' => $this->integer()->notNull()->comment('Компания'),
	        'finance_category_id' => $this->integer()->notNull()->comment('Категория'),
	        'parent_id' => $this->integer()->comment('Родительская статья'),

	        'name' => $this->string()->notNull()->comment('Название (Продажи, налоги, зарплата)'),
	        'type_id' => $this->integer()->notNull()->comment('Тип (Доходы расходы)'),
        ], $tableOptions);

	    //Юр. лицо
        $this->createTable('{{%finance_company}}', [
            'id' => $this->primaryKey(),
	        'finance_id' => $this->integer()->notNull()->comment('Компания'),
	        'name' => $this->string()->notNull()->comment('Название'),
	        'name_full' => $this->string()->comment('Полное название'),

            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),

	        'description' => $this->text()->comment('Описание'),
        ], $tableOptions);

	    //счет
        $this->createTable('{{%finance_bills}}', [
            'id' => $this->primaryKey(),
	        'finance_id' => $this->integer()->notNull()->comment('Компания'),
	        'finance_company_id' => $this->integer()->notNull()->comment('Юр. лицо'),
	        'name' => $this->string()->notNull()->comment('Название'),
	        'sum' => $this->decimal(9, 2)->notNull()->comment('Остаток'),
	        'sum_full' => $this->decimal(9, 2)->comment('Остаток'),

	        'bank_name' => $this->string()->comment('БИК'),
	        'bank_bic' => $this->string()->comment('БИК'),
	        'bank_number' => $this->string()->comment('БИК'),

            'currency_id' => $this->integer()->notNull()->comment('Валюта'),
            'data_point' => $this->date()->comment('Дата пересчета'),
	        'description' => $this->text()->comment('Описание'),
        ], $tableOptions);

	    //контрагент
        $this->createTable('{{%finance_contractor}}', [
            'id' => $this->primaryKey(),
	        'finance_id' => $this->integer()->notNull()->comment('Компания'),
	        'name' => $this->string()->notNull()->comment('Название'),
	        'name_full' => $this->string()->comment('Полное название'),

	        'inn' => $this->string()->comment('ИНН'),
	        'kpp' => $this->string()->comment('КПП'),

	        'description' => $this->text()->comment('Описание'),
        ], $tableOptions);

	    //Операции
        $this->createTable('{{%finance_transaction}}', [
            'id' => $this->primaryKey(),

	        'finance_id' => $this->integer()->notNull()->comment('Компания'),
	        'user_id' => $this->integer()->notNull()->comment('Пользователь'),
	        'finance_bills_id' => $this->integer()->notNull()->comment('Счет'),
	        'finance_contractor_id' => $this->integer()->notNull()->comment('Контрагент'),
	        'finance_expenditure_id' => $this->integer()->comment('Статья'),
	        'project_id' => $this->integer()->comment('Статья'),

	        'type_id' => $this->integer()->notNull()->comment('Тип (Поступление, выплата, перемещение, корректировка)'),

	        'sum' => $this->decimal(9, 2)->notNull()->comment('Сумма'),
	        'is_confirmation' => $this->smallInteger()->comment('Подтвкрждение'),
	        'date' => $this->date()->notNull()->comment('Дата'),

	        'description' => $this->text()->comment('Описание'),
        ], $tableOptions);

        $this->addForeignKey('{{%finance_user_id}}', '{{%finance}}', 'user_id', '{{%user}}', 'id');

	    $this->addForeignKey('{{%finance_category_fk1}}', '{{%finance_category}}', 'finance_id', '{{%finance}}', 'id');

	    $this->addForeignKey('{{%finance_expenditure_fk1}}', '{{%finance_expenditure}}', 'finance_category_id', '{{%finance_category}}', 'id');

	    $this->addForeignKey('{{%finance_company_fk1}}', '{{%finance_company}}', 'finance_id', '{{%finance}}', 'id');

	    $this->addForeignKey('{{%finance_bills_fk1}}', '{{%finance_bills}}', 'finance_id', '{{%finance}}', 'id');
	    $this->addForeignKey('{{%finance_bills_fk2}}', '{{%finance_bills}}', 'finance_company_id', '{{%finance_company}}', 'id');

	    $this->addForeignKey('{{%finance_contractor_fk1}}', '{{%finance_contractor}}', 'finance_id', '{{%finance}}', 'id');

	    $this->addForeignKey('{{%finance_transaction_fk1}}', '{{%finance_transaction}}', 'finance_id', '{{%finance}}', 'id');
	    $this->addForeignKey('{{%finance_transaction_fk2}}', '{{%finance_transaction}}', 'user_id', '{{%user}}', 'id');
	    $this->addForeignKey('{{%finance_transaction_fk3}}', '{{%finance_transaction}}', 'finance_bills_id', '{{%finance_bills}}', 'id');
	    $this->addForeignKey('{{%finance_transaction_fk4}}', '{{%finance_transaction}}', 'finance_contractor_id', '{{%finance_contractor}}', 'id');
	    $this->addForeignKey('{{%finance_transaction_fk5}}', '{{%finance_transaction}}', 'finance_expenditure_id', '{{%finance_expenditure}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%finance_transaction}}');
        $this->dropTable('{{%finance_contractor}}');
        $this->dropTable('{{%finance_bills}}');
        $this->dropTable('{{%finance_company}}');
        $this->dropTable('{{%finance_expenditure}}');
        $this->dropTable('{{%finance}}');
    }
}
