<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;


/**
 * Модуль работы с базой даннычх
 *
 * Class DbController
 * @package console\controllers
 */
class DbController extends Controller
{
	public static $version = '1.0';

	public function beforeAction($action)
	{
		$this->stdout("Модуль работы с базой даннычх (v".self::$version.")\n\n");

		return parent::beforeAction($action);
	}

	/**
	 * This command echoes what you have entered as the message.
	 */
	public function actionIndex()
	{
		echo "Команды \n\n";
		$this->stdout("php yii db/refresh\n", Console::FG_GREY);
		return self::EXIT_CODE_NORMAL;
	}


	public function actionRefresh()
	{
		$this->stdout("Start refresh\n", Console::FG_GREY);

		Yii::$app->cache->cachePath = 'backend/runtime/cache';
		Yii::$app->db->schema->refresh();

		Yii::$app->cache->cachePath = 'frontend/runtime/cache';
		Yii::$app->db->schema->refresh();

		$this->stdout("\nFinish refresh\n", Console::FG_GREY);

		return self::EXIT_CODE_NORMAL;
	}
}