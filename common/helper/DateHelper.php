<?php
/**
 * Created by PhpStorm.
 * User: Пушкин Дмитрий
 * Date: 26.10.2015 14:13
 */

namespace common\helper;

class DateHelper
{
	public static $translate = [
		"am" => "дп",
		"pm" => "пп",
		"AM" => "ДП",
		"PM" => "ПП",
		"Mon" => "Пн",
		"Tue" => "Вт",
		"Wed" => "Ср",
		"Thu" => "Чт",
		"Fri" => "Пт",
		"Sat" => "Сб",
		"Monday" => "Понедельник",
		"Tuesday" => "Вторник",
		"Wednesday" => "Среда",
		"Thursday" => "Четверг",
		"Friday" => "Пятница",
		"Saturday" => "Суббота",
		"Sunday" => "Воскресенье",
		"Sun" => "Вс",
		"Jan" => "Янв",
		"Feb" => "Фев",
		"Mar" => "Мар",
		"Apr" => "Апр",
		"May" => "Мая",
		"Jun" => "Июн",
		"Jul" => "Июл",
		"Aug" => "Авг",
		"Sep" => "Сен",
		"Oct" => "Окт",
		"Nov" => "Ноя",
		"Dec" => "Дек",
		"January" => "Января",
		"February" => "Февраля",
		"March" => "Марта",
		"July" => "Июля",
		"June" => "Июня",
		"April" => "Апреля",
		"August" => "Августа",
		"September" => "Сентября",
		"October" => "Октября",
		"November" => "Ноября",
		"December" => "Декабря",
		"st" => "ое",
		"nd" => "ое",
		"rd" => "е",
		"th" => "ое"
	];

	public static function getTimeFromDateTime($dateTime){
		$data = new \DateTime($dateTime);
		return $data->format('H:i');
	}

	public static function getDateFromDateTime($dateTime, $format = 'd F Y'){
		if(!$dateTime){
			return null;
		}

		$data = new \DateTime($dateTime);
		return strtr($data->format($format), self::$translate);
	}

	public static function getTimeFromMin($interval){
		$result = '';
		if(($interval/(60*24))>1) $result .= (int) ($interval/(60*24)) .' д ';
		if($interval/60>1) $result .= (int) ($interval/60)%24 .' ч ';
		if($interval%60) $result .= $interval%60 .' м';
		return $result;
	}

	public static function getDiffPeriod($dateTime = 'now', $period = 'P7D'){
		$data = new \DateTime($dateTime);
		$data->add(new \DateInterval($period));
		return $data > (new \DateTime('now'));
	}

	public static function getDiff($dateStart, $dateEnd){
		$data = new \DateTime($dateStart);
		$data2 = new \DateTime($dateEnd);
		return $data->diff($data2, true)->days;
	}

}