<?php
/**
 * Created by PhpStorm.
 * User: pushdmit
 * Date: 14.04.2016
 * Time: 20:12
 */

namespace common\helper;

class StatusHelper {

	private static $statuses = [
		0 => 'Не активный',
		1 => 'Активный',
		2 => 'На проверке',
		3 => 'Не оплачен',
		4 => 'В ожидании оплаты',
	];

	private static $is = [
		1 => 'Да',
		0 => 'Нет',
	];

	private static $priority = [
		1 => 'Немедленный',
		5 => 'Нормальный',
		10 => 'Низкий',
	];

	public static function getStatus()
	{
		return self::$statuses;
	}

	public static function getIs()
	{
		return self::$is;
	}

	public static function getPriority()
	{
		return self::$priority;
	}

	public static function getStatusName($name, $default = null)
	{
		if(isset(self::$statuses[$name]))
		{
			return \Yii::t('frontend', self::$statuses[$name]);
		}

		return $default;
	}

	public static function getIsName($name, $default = null)
	{
		if(isset(self::$is[$name]))
		{
			return \Yii::t('frontend', self::$is[$name]);
		}

		return $default;
	}

	public static function getPriorityName($name, $default = null)
	{
		if(isset(self::$priority[$name]))
		{
			return \Yii::t('frontend', self::$priority[$name]);
		}

		return $default;
	}

	public static function getStatusClass($id, $prefix = '')
	{
		return $prefix.'status_'.$id;
	}

}