<?php

namespace common\modules\finance\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "finance_category".
 *
 * @property integer $id
 * @property integer $finance_id
 * @property string $name
 * @property string $description
 *
 * @property Finance $finance
 * @property FinanceExpenditure[] $financeExpenditures
 */
class FinanceCategory extends \yii\db\ActiveRecord
{
	public static $defaultCategoryList = [
		[
			'name'=>'Операцоиная',
			'child' => [
				[
					'name' => 'Продажа товаров',
					'type_id' => 1,
				],
				[
					'name' => 'Оказание услуг',
					'type_id' => 1,
				],
				[
					'name' => 'Поставщики',
					'type_id' => 0,
				],
				[
					'name' => 'Маркетиг',
					'type_id' => 0,
				],
				[
					'name' => 'Офисные расходы',
					'type_id' => 0,
				],
				[
					'name' => 'Налоги',
					'type_id' => 0,
				],
				[
					'name' => 'Аутсорcинг',
					'type_id' => 0,
				],
				[
					'name' => 'Сотрудники',
					'type_id' => 0,
					'child' => [
						[
							'name' => 'Зарплата',
							'type_id' => 0,
						],
						[
							'name' => 'Налоги и взносы',
							'type_id' => 0,
						],
					]
				],
			],
		],
		['name'=>'Инвестиционная'],
		['name'=>'Финансовая'],
	];

	public static $categories;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['finance_id', 'name'], 'required'],
            [['finance_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['finance_id'], 'exist', 'skipOnError' => true, 'targetClass' => Finance::className(), 'targetAttribute' => ['finance_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'finance_id' => Yii::t('app', 'Компания'),
            'name' => Yii::t('app', 'Название (операцоиная, инвестиционная, Финансовая)'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    public function addChild($child, $parent_id = null){
	    foreach($child as $item){
		    $expenditure = new FinanceExpenditure();
		    $expenditure->load($item, '');
		    $expenditure->finance_id = $this->finance_id;
		    $expenditure->finance_category_id = $this->id;
		    $expenditure->parent_id = $parent_id;

		    if($expenditure->save()){
			    if(isset($item['child'])){
				    $this->addChild($item['child'], $expenditure->id);
			    }
		    }
	    }
    }
	public static function getList()
	{
		if(!self::$categories)
		{
			$select = [
				self::tableName().'.*',
			];

			self::$categories = self::find()
				->select($select)
				->all();
		}

		return self::$categories;
	}

	public static function getListMap()
	{
		return ArrayHelper::map(self::getList(), 'id', 'name');
	}
}
