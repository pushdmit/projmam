<?php

namespace common\modules\finance\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\finance\models\FinanceExpenditure;

/**
 * ExpenditureSearch represents the model behind the search form about `common\modules\finance\models\FinanceExpenditure`.
 */
class ExpenditureSearch extends FinanceExpenditure
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'finance_id', 'finance_category_id', 'parent_id', 'type_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceExpenditure::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'finance_id' => $this->finance_id,
            'finance_category_id' => $this->finance_category_id,
            'parent_id' => $this->parent_id,
            'type_id' => $this->type_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
