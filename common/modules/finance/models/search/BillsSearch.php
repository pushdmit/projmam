<?php

namespace common\modules\finance\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\finance\models\FinanceBills;

/**
 * BillsSearch represents the model behind the search form about `common\modules\finance\models\FinanceBills`.
 */
class BillsSearch extends FinanceBills
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'finance_id', 'finance_company_id', 'currency_id'], 'integer'],
            [['sum'], 'number'],
            [['name', 'bank_name', 'bank_bic', 'bank_number', 'data_point', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBills::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'finance_id' => $this->finance_id,
            'finance_company_id' => $this->finance_company_id,
            'currency_id' => $this->currency_id,
            'data_point' => $this->data_point,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_bic', $this->bank_bic])
            ->andFilterWhere(['like', 'bank_number', $this->bank_number])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
