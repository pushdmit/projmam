<?php

namespace common\modules\finance\models\search;

use common\modules\finance\models\FinanceBills;
use common\modules\finance\models\FinanceContractor;
use common\modules\finance\models\FinanceExpenditure;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\finance\models\FinanceTransaction;

/**
 * TransactionSearch represents the model behind the search form about `common\modules\finance\models\FinanceTransaction`.
 */
class TransactionSearch extends FinanceTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'finance_id', 'user_id', 'finance_bills_id', 'finance_contractor_id', 'finance_expenditure_id', 'project_id', 'type_id'], 'integer'],
            [['sum'], 'number'],
            [['is_confirmation', 'date', 'inn', 'kpp', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceTransaction::find();

		$select = [
			self::tableName().'.*',
			'ca.name finance_contractor_name',
			'b.name finance_bills_name',
			'e.name finance_expenditure_name',
		];
		$query->select($select);
		$query->leftJoin(FinanceContractor::tableName().' ca', 'ca.id = '.self::tableName().'.finance_contractor_id');
		$query->leftJoin(FinanceBills::tableName().' b', 'b.id = '.self::tableName().'.finance_bills_id');
		$query->leftJoin(FinanceExpenditure::tableName().' e', 'e.id = '.self::tableName().'.finance_expenditure_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
			self::tableName().'.id' => $this->id,
			self::tableName().'.finance_id' => $this->finance_id,
			self::tableName().'.user_id' => $this->user_id,
			self::tableName().'.finance_bills_id' => $this->finance_bills_id,
			self::tableName().'.finance_contractor_id' => $this->finance_contractor_id,
			self::tableName().'.finance_expenditure_id' => $this->finance_expenditure_id,
			self::tableName().'.project_id' => $this->project_id,
			self::tableName().'.type_id' => $this->type_id,
			self::tableName().'.sum' => $this->sum,
			self::tableName().'.date' => $this->date,
			self::tableName().'.is_confirmation' => $this->is_confirmation,
        ]);

        $query->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
