<?php

namespace common\modules\finance\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "finance_company".
 *
 * @property integer $id
 * @property integer $finance_id
 * @property string $name
 * @property string $name_full
 * @property string $inn
 * @property string $kpp
 * @property string $description
 *
 */
class FinanceCompany extends \yii\db\ActiveRecord
{
	public static $list;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['finance_id', 'name'], 'required'],
            [['finance_id'], 'integer'],
            [['description'], 'string'],
            [['name', 'name_full', 'inn', 'kpp'], 'string', 'max' => 255],
            [['finance_id'], 'exist', 'skipOnError' => true, 'targetClass' => Finance::className(), 'targetAttribute' => ['finance_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'finance_id' => Yii::t('app', 'Компания'),
            'name' => Yii::t('app', 'Название'),
            'name_full' => Yii::t('app', 'Полное название'),
            'inn' => Yii::t('app', 'ИНН'),
            'kpp' => Yii::t('app', 'КПП'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

	public function save($runValidation = true, $attributeNames = null)
	{
		if($this->isNewRecord){
			$this->finance_id = Yii::$app->request->get('id');
		}

		return parent::save($runValidation, $attributeNames);
	}

	public static function getList()
	{
		if(!self::$list)
		{
			$select = [
				self::tableName().'.*',
			];

			self::$list = self::find()
				->select($select)
				->all();
		}

		return self::$list;
	}

	public static function getListMap()
	{
		return ArrayHelper::map(self::getList(), 'id', 'name');
	}
}
