<?php

namespace common\modules\finance\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "finance_bills".
 *
 * @property integer $id
 * @property integer $finance_id
 * @property integer $finance_company_id
 * @property string $name
 * @property string $sum
 * @property string $sum_full
 * @property string $bank_name
 * @property string $bank_bic
 * @property string $bank_number
 * @property integer $currency_id
 * @property string $data_point
 * @property string $description
 *
 */
class FinanceBills extends \yii\db\ActiveRecord
{
	public static $list;
	public static $currencyList = [
		1 => 'Российский рубль'
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance_bills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['finance_id', 'finance_company_id', 'name', 'currency_id', 'sum', 'data_point'], 'required'],
            [['finance_id', 'finance_company_id', 'currency_id'], 'integer'],
            [['description'], 'string'],
            [['sum', 'sum_full'], 'number'],
            [['name', 'bank_name', 'bank_bic', 'bank_number'], 'string', 'max' => 255],
            [['finance_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceCompany::className(), 'targetAttribute' => ['finance_company_id' => 'id']],
            [['finance_id'], 'exist', 'skipOnError' => true, 'targetClass' => Finance::className(), 'targetAttribute' => ['finance_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'finance_id' => Yii::t('app', 'Компания'),
            'finance_company_id' => Yii::t('app', 'Юр. лицо'),
            'name' => Yii::t('app', 'Название'),
            'sum' => Yii::t('app', 'Остаток'),
            'sum_full' => Yii::t('app', 'Остаток'),
            'bank_name' => Yii::t('app', 'Банк'),
            'bank_bic' => Yii::t('app', 'БИК'),
            'bank_number' => Yii::t('app', 'Номер счета'),
            'currency_id' => Yii::t('app', 'Валюта'),
            'data_point' => Yii::t('app', 'Дата пересчета'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

	public function save($runValidation = true, $attributeNames = null)
	{
		if($this->isNewRecord){
			$this->finance_id = Yii::$app->request->get('id');
		}

		return parent::save($runValidation, $attributeNames);
	}

	public function reloadSum(){
		$this->sum_full = $this->sum+FinanceTransaction::find()
			->where(['finance_bills_id'=>$this->id])
			->andWhere(['>', 'date', $this->data_point])
			->sum('sum');
		$this->save();
	}

	public static function getList()
	{
		if(!self::$list)
		{
			$select = [
				self::tableName().'.*',
				'CONCAT(`name`, \' \', `sum`) name'
			];

			self::$list = self::find()
				->select($select)
				->all();
		}

		return self::$list;
	}

	public static function getListMap()
	{
		return ArrayHelper::map(self::getList(), 'id', 'name');
	}
}
