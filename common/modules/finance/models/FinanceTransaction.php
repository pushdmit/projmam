<?php

namespace common\modules\finance\models;

use Yii;

/**
 * This is the model class for table "finance_transaction".
 *
 * @property integer $id
 * @property integer $finance_id
 * @property integer $user_id
 * @property integer $finance_bills_id
 * @property integer $finance_contractor_id
 * @property integer $finance_expenditure_id
 * @property integer $project_id
 * @property integer $type_id
 * @property string $sum
 * @property string $is_confirmation
 * @property string $date
 * @property string $inn
 * @property string $kpp
 * @property string $description
 *
 */
class FinanceTransaction extends \yii\db\ActiveRecord
{
	public static $typeList = [
		1 => 'Поступление',
		2 => 'Выплата',
		3 => 'Перемещение',
//		4 => 'Корректировка',
	];

	public $finance_bills_to_id;

	public $finance_contractor_name;
	public $finance_bills_name;
	public $finance_expenditure_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['finance_id', 'user_id', 'finance_bills_id', 'type_id', 'sum', 'date'], 'required'],
            [['finance_id', 'user_id', 'finance_bills_id', 'finance_contractor_id', 'finance_expenditure_id', 'project_id', 'type_id'], 'integer'],
            [['sum'], 'number'],
            [['date'], 'safe'],
            [['description'], 'string'],
            [['is_confirmation', 'inn', 'kpp'], 'string', 'max' => 255],
            [['finance_expenditure_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceExpenditure::className(), 'targetAttribute' => ['finance_expenditure_id' => 'id']],
            [['finance_id'], 'exist', 'skipOnError' => true, 'targetClass' => Finance::className(), 'targetAttribute' => ['finance_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['finance_bills_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBills::className(), 'targetAttribute' => ['finance_bills_id' => 'id']],
            [['finance_contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceContractor::className(), 'targetAttribute' => ['finance_contractor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'finance_id' => Yii::t('app', 'Компания'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'finance_bills_id' => Yii::t('app', 'Счет'),
            'finance_bills_to_id' => Yii::t('app', 'Счет получатель'),
            'finance_bills_name' => Yii::t('app', 'Счет'),
            'finance_contractor_id' => Yii::t('app', 'Контрагент'),
            'finance_contractor_name' => Yii::t('app', 'Контрагент'),
            'finance_expenditure_id' => Yii::t('app', 'Статья'),
            'finance_expenditure_name' => Yii::t('app', 'Статья'),
            'project_id' => Yii::t('app', 'Проект'),
            'type_id' => Yii::t('app', 'Тип'),
            'sum' => Yii::t('app', 'Сумма'),
            'is_confirmation' => Yii::t('app', 'Подтвкрждение'),
            'date' => Yii::t('app', 'Дата'),
            'inn' => Yii::t('app', 'ИНН'),
            'kpp' => Yii::t('app', 'КПП'),
            'description' => Yii::t('app', 'Описание'),
        ];
    }

	public function save($runValidation = true, $attributeNames = null)
    {
	    if($this->isNewRecord){
		    $this->user_id = Yii::$app->getUser()->id;
		    $this->finance_id = Yii::$app->request->get('id');
	    }

	    if(!is_numeric($this->is_confirmation)){
			$this->is_confirmation = 1;
		}

		if(2 == $this->type_id){
			$this->sum = $this->sum*-1;
		}

		$result = parent::save($runValidation, $attributeNames);



		if($result){
			if(3 == $this->type_id && $this->finance_bills_to_id){
				$tr = new FinanceTransaction();
				$tr->load($this->attributes, '');
				$tr->sum = $tr->sum*-1;
				$tr->finance_bills_id = $this->finance_bills_to_id;
				$tr->save();
			}

			$bill = FinanceBills::findOne($this->finance_bills_id);
			$bill->reloadSum();
		}

	    return $result;
    }



	public static function getDataCart1()
	{
		$sql = 'SELECT t.`type_id`, YEAR(t.`date`) `year`, MONTH(t.`date`) `month`';
		$sql .= ', SUM(t.`sum`) planned';
		$sql .= ', SUM(CASE WHEN t.`is_confirmation` THEN t.`sum` END) factual';
		$sql .= ' FROM `finance_transaction` t';
		$sql .= ' INNER JOIN `finance_bills` b ON b.`id` = t.`finance_bills_id`';
		$sql .= ' WHERE t.`type_id` IN (1,2) AND t.`finance_id` = '.Yii::$app->request->get('id');
		$sql .= ' GROUP BY t.`type_id`, YEAR(t.`date`), MONTH(t.`date`)';
		$sql .= ' ORDER BY YEAR(t.`date`), MONTH(t.`date`), t.`type_id`';

		$result = self::findBySql($sql)->asArray()->all();

		foreach($result as &$item){
			if(2 == $item['type_id']){
				$item['factual'] *= -1;
				$item['planned'] *= -1;
			}
			$item['date'] = $item['month'].'.'.$item['year'];
			$item['type'] = self::$typeList[$item['type_id']];
			$item['color'] = 1==$item['type_id']?'#27b8f0':'#f1e522';
		}

//		var_dump($result); die;

		return $result;
	}

	public static function getDataBillCart($bill_id = null, $period = 'P1M')
	{
		$sql = 'SELECT t.`finance_bills_id`, b.`name`, t.`type_id`, YEAR(t.`date`) `year`, MONTH(t.`date`) `month`, DAY(t.`date`) `day`';
		$sql .= ', SUM(CASE WHEN t.`sum` > 0 THEN t.`sum` ELSE 0 END) coming_planned';
		$sql .= ', SUM(CASE WHEN t.`is_confirmation` AND t.`sum` > 0 THEN t.`sum` ELSE 0 END) coming_factual';
		$sql .= ', SUM(CASE WHEN t.`sum` < 0 THEN t.`sum` ELSE 0 END) output_planned';
		$sql .= ', SUM(CASE WHEN t.`is_confirmation` AND t.`sum` < 0 THEN t.`sum` ELSE 0 END) output_factual';
		$sql .= ' FROM `finance_transaction` t';
		$sql .= ' INNER JOIN `finance_bills` b ON b.`id` = t.`finance_bills_id`';
		$sql .= ' WHERE t.`finance_id` = '.Yii::$app->request->get('id');
		if($bill_id) $sql .= ' AND t.`finance_bills_id` = '.$bill_id;

		if('P1D' == $period){
			$sql .= ' GROUP BY t.`finance_bills_id`, t.`date`';
			$sql .= ' ORDER BY t.`date`, t.`type_id`';
		} else {
			$sql .= ' GROUP BY t.`finance_bills_id`, YEAR(t.`date`), MONTH(t.`date`)';
			$sql .= ' ORDER BY YEAR(t.`date`), MONTH(t.`date`), t.`type_id`';
		}


		$result = self::findBySql($sql)->asArray()->all();

		//todo получить баланс
		$balance = [
			1 => 4000,
			2 => 10000,
		];
		$balancePlan = [
			1 => 4000,
			2 => 10000,
		];

		foreach($result as &$item){
			if(!key_exists($item['finance_bills_id'], $balance)){
				$balance[$item['finance_bills_id']] = 0;
			}
			if(!key_exists($item['finance_bills_id'], $balancePlan)){
				$balancePlan[$item['finance_bills_id']] = 0;
			}
			$balance[$item['finance_bills_id']] += $item['output_factual']+$item['coming_factual'];
			$item['balance_factual'] = $balance[$item['finance_bills_id']];

			$balancePlan[$item['finance_bills_id']] += $item['output_planned']+$item['coming_planned'];
			$item['balance_planned'] = $balancePlan[$item['finance_bills_id']];

			$item['output_factual'] *= -1;
			$item['output_planned'] *= -1;

			$item['date'] = $item['month'].'.'.$item['year'];
			if('P1D' == $period){
				$item['date'] = $item['day'].'.'.$item['date'];
			}
			$item['type'] = $item['name'];
		}

//		var_dump($result); die;

		return $result;
	}

	public static function getDataCart2($type_id = 1)
	{
		$sql = 'SELECT t.`finance_expenditure_id`, e.`name`, t.`type_id`, YEAR(t.`date`) `year`, MONTH(t.`date`) `month`';
		$sql .= ', SUM(t.`sum`) planned, SUM(CASE WHEN t.`is_confirmation` THEN t.`sum` END) factual';
		$sql .= ' FROM `finance_transaction` t';
		$sql .= ' INNER JOIN `finance_expenditure` e ON e.`id` = t.`finance_expenditure_id`';
		$sql .= ' WHERE t.`type_id` = '.$type_id.' AND t.`finance_id` = '.Yii::$app->request->get('id');
		$sql .= ' GROUP BY t.`finance_expenditure_id`, t.`type_id`, YEAR(t.`date`), MONTH(t.`date`)';
		$sql .= ' ORDER BY YEAR(t.`date`), MONTH(t.`date`), t.`type_id`';

		$result = self::findBySql($sql)->asArray()->all();

		foreach($result as &$item){
			if(2 == $item['type_id']){
				$item['factual'] *= -1;
				$item['planned'] *= -1;
			}
			$item['date'] = $item['month'].'.'.$item['year'];
			$item['type'] = $item['name'];
			$item['color'] = 1==$item['type_id']?'#27b8f0':'#f1e522';
		}

//		var_dump($result); die;

		return $result;
	}
}
