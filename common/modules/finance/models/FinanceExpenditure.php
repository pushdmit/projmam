<?php

namespace common\modules\finance\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "finance_expenditure".
 *
 * @property integer $id
 * @property integer $finance_id
 * @property integer $finance_category_id
 * @property integer $parent_id
 * @property string $name
 * @property integer $type_id
 *
 * @property FinanceCategory $financeCategory
 * @property FinanceTransaction[] $financeTransactions
 */
class FinanceExpenditure extends \yii\db\ActiveRecord
{
	public static $typeList = [
		1 => 'Доходы',
		2 => 'Расходы',
	];

	public static $expenditure;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance_expenditure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['finance_id', 'finance_category_id', 'name', 'type_id'], 'required'],
            [['finance_id', 'finance_category_id', 'parent_id', 'type_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['finance_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceCategory::className(), 'targetAttribute' => ['finance_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'finance_id' => Yii::t('app', 'Компания'),
            'finance_category_id' => Yii::t('app', 'Категория'),
            'parent_id' => Yii::t('app', 'Родительская статья'),
            'name' => Yii::t('app', 'Название'),
            'type_id' => Yii::t('app', 'Тип (Доходы расходы)'),
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
	    if($this->isNewRecord){
		    $this->finance_id = Yii::$app->request->get('id');
	    }

	    return parent::save($runValidation, $attributeNames);
    }


	public static function getList()
	{
		if(!self::$expenditure)
		{
			$select = [
				self::tableName().'.*',
			];

			self::$expenditure = self::find()
				->select($select)
				->all();
		}

		return self::$expenditure;
	}

	public static function getListMap()
	{
		return ArrayHelper::map(self::getList(), 'id', 'name');
	}
}
