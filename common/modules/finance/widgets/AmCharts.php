<?php

namespace common\modules\finance\widgets;

use common\modules\finance\assets\AmChartAsset;
use Yii;
use yii\helpers\Html;
use yii\web\View;
use yii\base\InvalidConfigException;

class AmCharts extends \yii\bootstrap\Widget
{
	public $options = [];
	public $width = '570px';
	public $height = '360px';
	public $language;

	public $chartConfiguration = [];
	protected $_chartId;

	public function init()
	{
		if(!isset($this->options['id'])) {
			$this->options['id'] = 'chart_' . $this->getId();
		}
		$this->chartId = $this->options['id'];
		parent::init();
	}

	public function run()
	{
		$this->makeChart();
		$this->options['style'] = "width: {$this->width}; height: {$this->height};";
		echo Html::tag('div', '', $this->options);
	}

	protected function makeChart()
	{
		if (!isset($this->chartConfiguration['language'])) {
			$this->chartConfiguration['language'] = $this->language ? $this->language : Yii::$app->language;
		}

		$assetBundle = AmChartAsset::register($this->getView());
		if (isset($this->chartConfiguration['type'])) {
			$assetBundle->addTypeJs($this->chartConfiguration['type']);
		}
		if (isset($this->chartConfiguration['theme'])) {
			$assetBundle->addThemeJs($this->chartConfiguration['theme']);
		}
		if (isset($this->chartConfiguration['amExport'])) {
			$assetBundle->addExportJs();
		}
		$assetBundle->addLanguageJs($this->chartConfiguration['language']);
		if (!isset($this->chartConfiguration['pathToImages'])) {
			$this->chartConfiguration['pathToImages'] = $assetBundle->baseUrl . '/images/';
		}
		$chartConfiguration = json_encode($this->chartConfiguration);
		$js = $this->chartId . " = AmCharts.makeChart('{$this->chartId}', {$chartConfiguration});";
		$this->getView()->registerJs($js, View::POS_READY);
	}

	protected function setChartId($value)
	{
		$this->_chartId = $value;
	}

	protected function getChartId()
	{
		return $this->_chartId;
	}
}
