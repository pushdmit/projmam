<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\finance\models;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceTransaction */
/* @var $form yii\widgets\ActiveForm */

$def = [''=>'Выбрать']
?>

<div class="finance-transaction-form">

    <?php $form = ActiveForm::begin([
		'options' => [
//			'class' => 'form-horizontal',
		],
	]); ?>

	<?= $form->field($model, 'type_id')->dropDownList(models\FinanceTransaction::$typeList) ?>

	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
				'options' => [
					'class' => 'form-control',
				],
				'value' => $model->date,
				'language' => Yii::$app->language,
				'dateFormat' => 'yyyy-MM-dd',
			]) ?>
		</div>

		<div class="col-md-6">
			<?= $form->field($model, 'is_confirmation')->checkbox() ?>
		</div>
	</div>

    <?= $form->field($model, 'finance_bills_id')->dropDownList(models\FinanceBills::getListMap()) ?>

	<?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'finance_bills_to_id', [
		'options' => [
			'class' => 'div-hidden',
			'data-view-type' => '3',
		],
	])->dropDownList($def+models\FinanceBills::getListMap()) ?>

    <?= $form->field($model, 'finance_contractor_id', [
		'options' => [
			'data-view-type' => '1,2',
		],
	])->dropDownList($def+models\FinanceContractor::getListMap()) ?>

    <?= $form->field($model, 'finance_expenditure_id', [
		'options' => [
			'data-view-type' => '1,2',
		],
	])->widget(\kartik\select2\Select2::classname(), [
		'data' => $def+models\FinanceExpenditure::getListMap(),
		'language' => 'ru',
		'options' => ['placeholder' => 'Выберите статью ...'],
		'pluginOptions' => [
			'allowClear' => true
		],
	])//->dropDownList($def+models\FinanceExpenditure::getListMap()) ?>

    <?//= $form->field($model, 'project_id')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 1]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<style>
	.div-hidden{
		display: none;
	}
</style>

<script type="application/javascript">
	$(document).ready(function () {
		$('#financetransaction-type_id').change(function(){
			var element = $('[data-view-type]');
			var type = $(this).val();

			element.css('display', 'none');

			element.each(function(key, label){
				if($(label).attr('data-view-type').indexOf(type)+1){
					$(label).show();
				} else {
					$(label).find(':input').val('');
				}
			});
		});
	});
</script>
