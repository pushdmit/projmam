<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceTransaction */
?>
<div class="finance-transaction-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'finance_id',
            'user_id',
            'finance_bills_id',
            'finance_contractor_id',
            'finance_expenditure_id',
            'project_id',
            'type_id',
            'sum',
            'is_confirmation',
            'date',
            'inn',
            'kpp',
            'description:ntext',
        ],
    ]) ?>

</div>
