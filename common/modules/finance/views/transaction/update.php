<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceTransaction */
?>
<div class="finance-transaction-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
