<?php

use yii\bootstrap\Modal;
use common\modules\finance\widgets\AmCharts;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\finance\models\search\ChartSearch */
/* @var $dataProvider [] */
/* @var $dataProvider2 [] */
/* @var $dataProvider3 [] */
/* @var $dataProvider4 [] */
/* @var $dataProvider5 [] */
/* @var $graphs [] */

$script = <<< JS
$('#chart-bill-search').find(':input').change(function(){
	var a = $('#billReloadCart');
	var href = a.attr('data-href')+'&ChartSearch[finance_bills_id]='+$('#chartsearch-finance_bills_id').val();
	href = href+'&ChartSearch[period]='+$('#chartsearch-period').val();
	// console.log(href);
	a.attr('href', href).click();
});
JS;
$this->registerJs($script);

CrudAsset::register($this);

$this->title = 'Графики';
$def = [''=>'Выбрать']
?>
<?= $this->render('../default/_menu') ?>

<div class="charts-index">
	<div class="row">
		<div class="col-md-6">

		</div>

		<div class="col-md-6">
			<?php
			$chartConfiguration = [
				'type' => 'serial',
				'theme' => 'light',
				'depth3D' => 60,
				'angle' => 30,
				'startDuration' => 1,
				'dataProvider' => $dataProvider,
				'valueAxes' => [
					[
						'title' => 'GDP growth rate',
						'stackType' => '3d',
						'unit' => ' p.',
						'position' => 'left',
					]
				],
				'graphs' => [
					[
						'type' => 'column',
						'title' => '[[type]]',
						'valueField' => 'factual',
						'fillAlphas' => 0.8,
						'lineAlpha' => 0.3,
						'fillColorsField' => 'color',
						'balloonText' => '[[type]] за [[category]]:<b>[[value]]</b>'
					],
					[
						'type' => 'column',
						'title' => '[[type]]',
						'valueField' => 'planned',
						'fillAlphas' => 0.4,
						'lineAlpha' => 0.1,
						'fillColorsField' => 'color',
						'balloonText' => 'План "[[type]]" за [[category]]:<b>[[value]]</b>'
					],
				],
				'categoryField' => 'date',
				'categoryAxis' => [
					'gridPosition' => 'start',
				],
				'export' => [
					'enabled' => true,
				],
			];
			?>
			<?= \common\modules\finance\widgets\AmCharts::widget(['chartConfiguration' => $chartConfiguration]); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<?php
			$chartConfiguration = [
				'type' => 'pie',
				'theme' => 'light',
				'startDuration' => 0,
				'addClassNames' => true,
				'legend'=>[
					'position'=>'right',
					'marginRight'=>90,
					'autoMargins'=>true,
				],
				'dataProvider' => $dataProvider2,
				'titleField' => 'name',
				'valueField' => 'factual',
				'labelRadius' => 5,
				'radius' => '40%',
				'innerRadius' => '45%',
				'labelText' => '[[name]]',
				'defs' => [
					'filter' => [
						[
							'id' => 'shadow',
							'width' => '200%',
							'height' => '200%',
						],
					]
				],
				'export' => [
					'enabled' => true,
				],
			];
			?>
			<?= \common\modules\finance\widgets\AmCharts::widget(['chartConfiguration' => $chartConfiguration]); ?>
		</div>

		<div class="col-md-6">
			<?php
			$chartConfiguration = [
				'type' => 'serial',
				'theme' => 'light',
				'depth3D' => 60,
				'angle' => 30,
				'startDuration' => 1,
				'dataProvider' => $dataProvider2,
				'valueAxes' => [
					[
						'title' => 'GDP growth rate',
						'stackType' => '3d',
						'unit' => ' p.',
						'position' => 'left',
					]
				],
				'graphs' => [
					[
						'type' => 'column',
						'title' => '[[type]]',
						'valueField' => 'factual',
						'fillAlphas' => 0.8,
						'lineAlpha' => 0.3,
						'fillColorsField' => 'color',
						'balloonText' => '[[type]] за [[category]]:<b>[[value]]</b>'
					],
					[
						'type' => 'column',
						'title' => '[[type]]',
						'valueField' => 'planned',
						'fillAlphas' => 0.4,
						'lineAlpha' => 0.1,
						'fillColorsField' => 'color',
						'balloonText' => 'План "[[type]]" за [[category]]:<b>[[value]]</b>'
					],
				],
				'categoryField' => 'date',
				'categoryAxis' => [
					'gridPosition' => 'start',
				],
				'export' => [
					'enabled' => true,
				],
			];
			?>
			<?= \common\modules\finance\widgets\AmCharts::widget(['chartConfiguration' => $chartConfiguration]); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<?php
			$chartConfiguration = [
				'type' => 'pie',
				'theme' => 'light',
				'legend'=>[
					'position'=>'right',
					'marginRight'=>90,
					'autoMargins'=>true,
				],
				'dataProvider' => $dataProvider3,
				'titleField' => 'name',
				'valueField' => 'factual',
				'labelRadius' => 5,
				'radius' => '40%',
				'innerRadius' => '45%',
				'labelText' => '[[name]]',
				'export' => [
					'enabled' => true,
				],
			];
			?>
			<?= \common\modules\finance\widgets\AmCharts::widget(['chartConfiguration' => $chartConfiguration]); ?>
		</div>

		<div class="col-md-6">
			<?php
			$chartConfiguration = [
				'type' => 'serial',
				'theme' => 'light',
				'depth3D' => 60,
				'angle' => 30,
				'startDuration' => 1,
				'dataProvider' => $dataProvider3,
				'valueAxes' => [
					[
						'title' => 'GDP growth rate',
						'stackType' => '3d',
						'unit' => ' p.',
						'position' => 'left',
					]
				],
				'graphs' => [
					[
						'type' => 'column',
						'title' => '[[type]]',
						'valueField' => 'factual',
						'fillAlphas' => 0.8,
						'lineAlpha' => 0.3,
						'fillColorsField' => 'color',
						'balloonText' => '[[type]] за [[category]]:<b>[[value]]</b>'
					],
					[
						'type' => 'column',
						'title' => '[[type]]',
						'valueField' => 'planned',
						'fillAlphas' => 0.4,
						'lineAlpha' => 0.1,
						'fillColorsField' => 'color',
						'balloonText' => 'План "[[type]]" за [[category]]:<b>[[value]]</b>'
					],
				],
				'categoryField' => 'date',
				'categoryAxis' => [
					'gridPosition' => 'start',
				],
				'export' => [
					'enabled' => true,
				],
			];
			?>
			<?= AmCharts::widget(['chartConfiguration' => $chartConfiguration]); ?>
		</div>
	</div>

	<div class="row">
		<?php
		$chartConfiguration = [
			'type' => 'serial',
			'theme' => 'light',
			'dataDateFormat' => 'MM.YYYY',
			'precision' => 2,
			'valueAxes' => [
				[
					'id' => 'v1',
					'title' => 'Приход/Расход',
					'position' => 'left',
					'autoGridCount' => false,
				],
			],
			'graphs' => [
				[
					'id' => 'g1',
					'valueAxis' => 'v1',
					'type' => 'column',
					'title' => '[[type]]',
					'valueField' => 'coming_factual',
					'fillAlphas' => 1,
					'lineAlpha' => 0.3,
					'clustered' => false,
					'fillColors' => '#27b8f0',
					'columnWidth' => 0.5,
					'balloonText' => '[[type]] прибыль за [[category]]:<b>[[value]]</b>'
				],
				[
					'id' => 'g3',
					'valueAxis' => 'v1',
					'type' => 'column',
					'title' => '[[type]]',
					'valueField' => 'output_factual',
					'fillAlphas' => 0.8,
					'lineAlpha' => 0.3,
					'clustered' => false,
					'fillColors' => '#f1e522',
					'columnWidth' => 0.5,
					'balloonText' => '[[type]] затраты за [[category]]:<b>[[value]]</b>'
				],
				[
					'id' => 'g2',
					'valueAxis' => 'v1',
					'type' => 'column',
					'title' => '[[type]]',
					'valueField' => 'coming_planned',
					'fillAlphas' => 0.6,
					'lineAlpha' => 0.1,
					'clustered' => false,
					'fillColors' => '#27b8f0',
					'columnWidth' => 0.5,
					'balloonText' => 'План "[[type]]" прибыль за [[category]]:<b>[[value]]</b>'
				],
				[
					'id' => 'g4',
					'valueAxis' => 'v1',
					'type' => 'column',
					'title' => '[[type]]',
					'valueField' => 'output_planned',
					'fillAlphas' => 0.4,
					'lineAlpha' => 0.1,
					'clustered' => false,
					'fillColors' => '#f1e522',
					'columnWidth' => 0.5,
					'balloonText' => 'План "[[type]]" затраты за [[category]]:<b>[[value]]</b>'
				]
			],
			'dataProvider' => $dataProvider4,
			'categoryField' => 'date',
			'chartCursor' => [
				'pan' => true,
				'valueLineEnabled' => true,
				'valueLineBalloonEnabled' => true,
				'cursorAlpha' => 0,
				'valueLineAlpha' => 0.2,
			],
			'categoryAxis' => [
				'gridPosition' => 'start',
			],
			'export' => [
				'enabled' => true,
			],
		];
		?>
		<?= AmCharts::widget(['chartConfiguration' => $chartConfiguration, 'width'=>'1140px']); ?>
	</div>

	<div class="row">
		<div id="chart-bill-search">
			<?php $form = ActiveForm::begin([
				'action' => ['chart'],
				'method' => 'get',
				'options' => [
					'data-pjax' => 'true'
				],
			]); ?>

			<div class="row">
				<div class="col-md-3">
					<?= $form->field($searchModel, 'finance_bills_id')
						->dropDownList(\common\modules\finance\models\FinanceBills::getListMap()) ?>
				</div>
				<div class="col-md-3">
					<?= $form->field($searchModel, 'period')
						->dropDownList(\common\modules\finance\models\search\ChartSearch::$periodList) ?>
				</div>
			</div>

			<input type="submit" id="sendButton" style="display: none;">
			<?php ActiveForm::end(); ?>
		</div>

		<?= $this->render('chart', ['dataProvider'=>$dataProvider5, 'chart_id' => 'bill']) ?>
	</div>
</div>

<?php Modal::begin([
	"id" => "ajaxCrudModal",
	"footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<style>
	/*#chartdiv {*/
		/*width		: 100%;*/
		/*height		: 500px;*/
		/*font-size	: 11px;*/
	/*}*/
</style>
