<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceContractor */
?>
<div class="finance-contractor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
