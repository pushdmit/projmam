<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceContractor */
?>
<div class="finance-contractor-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'finance_id',
            'name',
            'name_full',
            'inn',
            'kpp',
            'description:ntext',
        ],
    ]) ?>

</div>
