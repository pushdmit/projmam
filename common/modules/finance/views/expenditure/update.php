<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceExpenditure */
?>
<div class="finance-expenditure-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
