<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\finance\models;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceExpenditure */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-expenditure-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'type_id')->dropDownList(models\FinanceExpenditure::$typeList) ?>

    <?= $form->field($model, 'finance_category_id')->dropDownList(models\FinanceCategory::getListMap()) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
