<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceExpenditure */
?>
<div class="finance-expenditure-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'finance_id',
            'finance_category_id',
            'parent_id',
            'name',
            'type_id',
        ],
    ]) ?>

</div>
