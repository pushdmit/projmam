<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */
?>

<div class="btn-group" role="group" aria-label="..." style="padding:0 0 30px">
    <?= Html::a(
    	Yii::t('finance', 'Операции'),
		['/finance/'.Yii::$app->request->get('id').'/transaction'],
		['class' => 'btn btn-default']
    ) ?>

	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('frontend', 'Статьи расходов')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('finance', 'Статьи'), ['/finance/'.Yii::$app->request->get('id').'/expenditure']) ?>
			</li>
			<li>
				<?= Html::a(
					Yii::t('finance', 'Новая статья'),
					['/finance/'.Yii::$app->request->get('id').'/expenditure/create'],
					['role' => 'modal-remote']
				) ?>
			</li>
		</ul>
	</div>

	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('frontend', 'Юр.лицо')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('finance', 'Юр.лицо'), ['/finance/'.Yii::$app->request->get('id').'/company']) ?>
			</li>
			<li>
				<?= Html::a(
					Yii::t('finance', 'Новое Юр.лицо'),
					['/finance/'.Yii::$app->request->get('id').'/company/create'],
					['role' => 'modal-remote']
				) ?>
			</li>
		</ul>
	</div>

	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('frontend', 'Счета')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('finance', 'Счета'), ['/finance/'.Yii::$app->request->get('id').'/bills']) ?>
			</li>
			<li>
				<?= Html::a(
					Yii::t('finance', 'Новый счет'),
					['/finance/'.Yii::$app->request->get('id').'/bills/create'],
					['role' => 'modal-remote']
				) ?>
			</li>
		</ul>
	</div>

	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('frontend', 'Контрагент')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('finance', 'Контрагент'), ['/finance/'.Yii::$app->request->get('id').'/contractor']) ?>
			</li>
			<li>
				<?= Html::a(
					Yii::t('finance', 'Новый контрагент'),
					['/finance/'.Yii::$app->request->get('id').'/contractor/create'],
					['role' => 'modal-remote']
				) ?>
			</li>
		</ul>
	</div>

    <?= Html::a(
    	Yii::t('finance', 'Календарь'),
	    ['/finance/'.Yii::$app->request->get('id').'/calendar'],
	    ['class' => 'btn btn-default']
    ) ?>

    <?= Html::a(
    	Yii::t('finance', 'Графики'),
	    ['/finance/'.Yii::$app->request->get('id').'/charts'],
	    ['class' => 'btn btn-default']
    ) ?>

	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('frontend', 'Управление')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('finance', 'Manager'), ['/project/'.Yii::$app->request->get('id').'/manager']) ?>
			</li>
		</ul>
	</div>
</div>
