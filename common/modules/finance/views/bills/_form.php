<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\finance\models;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceBills */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-bills-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'finance_company_id')->dropDownList(models\FinanceCompany::getListMap()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>
		</div>

		<div class="col-md-6">
			<?= $form->field($model, 'data_point')->widget(\yii\jui\DatePicker::classname(), [
				'options' => [
					'class' => 'form-control',
				],
				'value' => $model->data_point,
				'language' => Yii::$app->language,
				'dateFormat' => 'yyyy-MM-dd',
			]) ?>
		</div>
	</div>

    <?//= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'bank_bic')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'bank_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency_id')->dropDownList(models\FinanceBills::$currencyList) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('finance', 'Create') : Yii::t('finance', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
