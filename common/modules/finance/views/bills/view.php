<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceBills */
?>
<div class="finance-bills-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'finance_id',
            'finance_company_id',
            'name',
            'bank_name',
            'bank_bic',
            'bank_number',
            'currency_id',
            'data_point',
            'description:ntext',
        ],
    ]) ?>

</div>
