<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceBills */
?>
<div class="finance-bills-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
