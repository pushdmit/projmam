<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\finance\models\FinanceCompany */

?>
<div class="finance-company-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
