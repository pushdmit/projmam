<?php

namespace common\modules\finance\controllers;

use common\models\search\FinanceSearch;
use yii\web\Controller;

/**
 * Default controller for the `modules` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
	    $searchModel = new FinanceSearch();
	    $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

	    return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
	    ]);
    }
}
