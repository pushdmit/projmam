<?php

namespace common\modules\finance\controllers;

use common\modules\finance\models\FinanceTransaction;
use common\modules\finance\models\search\ChartSearch;
use yii\web\Controller;

/**
 * Default controller for the `modules` module
 */
class ChartsController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
	    $searchModel = new ChartSearch();
		$searchModel->search(\Yii::$app->request->queryParams);
		if(!$searchModel->finance_bills_id){
			$searchModel->finance_bills_id = 1;
		}

	    $dataProvider = FinanceTransaction::getDataCart1();
	    $dataProvider2 = FinanceTransaction::getDataCart2();
	    $dataProvider3 = FinanceTransaction::getDataCart2(2);
	    $dataProvider4 = FinanceTransaction::getDataBillCart();
	    $dataProvider5 = FinanceTransaction::getDataBillCart($searchModel->finance_bills_id);

	    return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'dataProvider2' => $dataProvider2,
		    'dataProvider3' => $dataProvider3,
		    'dataProvider4' => $dataProvider4,
		    'dataProvider5' => $dataProvider5,
	    ]);
    }
    public function actionChart()
    {
	    $searchModel = new ChartSearch();
		$searchModel->search(\Yii::$app->request->queryParams);
		if(!$searchModel->finance_bills_id){
			$searchModel->finance_bills_id = 1;
		}

	    $dataProvider = FinanceTransaction::getDataBillCart($searchModel->finance_bills_id, $searchModel->period);

		if(\Yii::$app->request->isAjax){
			return $this->renderPartial('chart', [
				'chart_id' => 'bill',
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]);
		}

	    return $this->render('chart', [
		    'chart_id' => 'bill',
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
	    ]);
    }
}
