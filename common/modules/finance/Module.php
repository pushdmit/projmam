<?php

namespace common\modules\finance;
use yii\web\UrlManager;

/**
 * modules module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\finance\controllers';

	public $addDefaultUrlRules = true;

	public $defaultUrlRules = [
		'<module:(finance)>/<id>' => '<module>/transaction/index',
		'<module:(finance)>/<id>/<controler>' => '<module>/<controler>/index',
		'<module:(finance)>/<id>/<controler>/<action>' => '<module>/<controler>/<action>',
	];

	public function bootstrap($app)
	{
		/** @var UrlManager $urlManager */
		$urlManager = $app->urlManager;

		if ($this->addDefaultUrlRules && !empty($this->defaultUrlRules)) {
//			var_dump($app->urlManager); die;
			$urlManager->addRules($this->defaultUrlRules);
		}
	}

    public function init()
    {
		$this->bootstrap(\Yii::$app);

        parent::init();
    }
}
