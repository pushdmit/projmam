<?php

namespace common\modules\wiki\controllers;

use yii\web\Controller;

/**
 * Default controller for the `wiki` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionWidget()
    {
        return $this->render('widget');
    }

    public function actionBrowse()
    {
        return $this->render('browse');
    }

    public function actionUpload()
    {
		$dir = $this->getDir();

		if(!file_exists($dir)){
			mkdir($dir);
		}

		$name = md5($_FILES['upload']['name']).'.'.substr($_FILES['upload']['name'], -3);
		$uploadfile = $dir.'/'.$name;
		move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile);
		$name = '/upload/wiki/'.\Yii::$app->request->get('id').'/'.$name;

        echo '<script type="text/javascript">
    		window.parent.CKEDITOR.tools.callFunction("0", "'.$name.'", "");
		</script>';
    }

    public function getDir()
    {
        return \Yii::getAlias('@frontend').'/web/upload/wiki/'.\Yii::$app->request->get('id');
    }
}
