<?php

namespace common\modules\wiki\controllers;

use common\modules\wiki\models\WikiArticleLanguage;
use Yii;
use common\modules\wiki\models\WikiArticle;
use common\modules\wiki\models\search\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ArticleController implements the CRUD actions for WikiArticle model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
	{
//		var_dump(Yii::getAlias('@frontend')); die;

//		$_SESSION['KCFINDER'] = array(
//			'disabled' => false,
//			'uploadURL' => '/upload/wiki/'.Yii::$app->request->get('module', 'project').'/'.Yii::$app->request->get('element_id', 'main'),
//			'uploadDir' => ""
//		);

		Yii::$app->session->set('KCFINDER', [
			'uploadURL'=>'/upload/wiki/'.Yii::$app->request->get('module', 'project').'/'.Yii::$app->request->get('element_id', 'main'),
			'uploadDir'=>Yii::getAlias('@frontend').'/web/upload/wiki/'.Yii::$app->request->get('module', 'project').'/'.Yii::$app->request->get('element_id', 'main'),
		]);

		return parent::beforeAction($action); // TODO: Change the autogenerated stub
	}

	/**
     * Lists all WikiArticle models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single WikiArticle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;

		$params = [
			'model' => $this->findModel($id, $request->get('language', Yii::$app->language)),
		];

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
				'title'=> "WikiArticle #".$id,
				'size'=> "large",
				'content'=>$this->renderAjax('view', $params),
				'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
					Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
			];
        }

		return $this->render('view', $params);
    }

    /**
     * Creates a new WikiArticle model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new WikiArticle();
		$modelLanguage = new WikiArticleLanguage();
		$modelLanguage->language = $request->get('language', Yii::$app->language);

		$params = [
			'model' => $model,
			'modelLanguage' => $modelLanguage,
		];

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new WikiArticle",
					'size'=> "large",
                    'content'=>$this->renderAjax('create', $params),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){

				$model->saveLanguage($modelLanguage, $request->post());

				return [
					'forceReload'=>'#crud-datatable-pjax',
					'size'=> "large",
					'title'=> "Create new WikiArticle",
					'content'=>'<span class="text-success">Create WikiArticle success</span>',
					'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
				];
            }else{           
                return [
                    'title'=> "Create new WikiArticle",
					'size'=> "large",
                    'content'=>$this->renderAjax('create', $params),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save())
            {
				$model->saveLanguage($modelLanguage, $request->post());

				return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', $params);
            }
        }
    }

    /**
     * Updates an existing WikiArticle model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
		$modelLanguage = WikiArticleLanguage::find()
			->where(['wiki_article_id'=>$model->id, 'language'=>$request->get('language', Yii::$app->language)])
			->one();

		if(!$modelLanguage){
			$modelLanguage = new WikiArticleLanguage();
			$modelLanguage->language = $request->get('language', Yii::$app->language);
		}

		$params = [
			'model' => $model,
			'modelLanguage' => $modelLanguage,
		];

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update WikiArticle #".$id,
					'size'=> "large",
                    'content'=>$this->renderAjax('update', $params),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){

				$model->saveLanguage($modelLanguage, $request->post());

                return [
                    'forceReload'=>'#crud-datatable-pjax',
					'size'=> "large",
                    'title'=> "WikiArticle #".$id,
                    'content'=>$this->renderAjax('view', $params),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update WikiArticle #".$id,
					 'size'=> "large",
                    'content'=>$this->renderAjax('update', $params),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($model->load($request->post()) && $model->save())
            {
				$model->saveLanguage($modelLanguage, $request->post());

                return $this->redirect(['view', 'id' => $model->id, 'language'=>$request->get('language', Yii::$app->language)]);
            } else {
                return $this->render('update', $params);
            }
        }
    }

    /**
     * Delete an existing WikiArticle model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing WikiArticle model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the WikiArticle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $language
     * @return WikiArticle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
	protected function findModel($id, $language = null)
	{
		if (($model = WikiArticle::findOneFull($id, $language)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
