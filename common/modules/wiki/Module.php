<?php

namespace common\modules\wiki;

/**
 * wiki module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\wiki\controllers';

    public $languages = [
		'ru' => 'Русский',
		'en' => 'English',
	];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
