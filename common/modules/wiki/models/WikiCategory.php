<?php

namespace common\modules\wiki\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "wiki_category".
 *
 * @property integer $id
 * @property string $module
 * @property string $element
 * @property string $name
 */
class WikiCategory extends \yii\db\ActiveRecord
{
	public static $list;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wiki_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'element', 'name'], 'required'],
            [['module', 'element', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'module' => Yii::t('app', 'Модуль'),
            'element' => Yii::t('app', 'Элемент'),
            'name' => Yii::t('app', 'Название'),
        ];
    }

    public static function getWhere(){
		$where = [];

		if(Yii::$app->request->get('module')){
			$where['module'] = Yii::$app->request->get('module');
		}

		if(Yii::$app->request->get('element_id')){
			$where['element'] = Yii::$app->request->get('element_id');
		}

		return $where;
	}

    public static function getCategoryForModule($module = null, $element = null){
		$where = [];

		if($module){ $where['module'] = $module;}
		if($element){ $where['element'] = $element; }

		$category = WikiCategory::find();

		if($where){
			$category->where($where);
		}

		return $category->all();
	}

	public static function getList()
	{
		if(!self::$list){
			$select = [
				self::tableName() . '.*'
			];

			$query = self::find()->select($select);

			$where = self::getWhere();
			if($where){
				$query->where($where);
			}

			self::$list = $query->all();
		}

		return self::$list;
	}

	public static function getListMap()
	{
		return ArrayHelper::map(self::getList(), 'id', 'name');
	}

}
