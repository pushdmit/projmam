<?php

namespace common\modules\wiki\models;

use Yii;

/**
 * This is the model class for table "wiki_article_language".
 *
 * @property integer $id
 * @property integer $wiki_article_id
 * @property string $language
 * @property string $content
 */
class WikiArticleLanguage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wiki_article_language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wiki_article_id', 'language', 'content'], 'required'],
            [['wiki_article_id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 255],
			[
				['wiki_article_id', 'language'], 'unique', 'targetAttribute' => ['wiki_article_id', 'language'],
				'message' => 'The combination of "Статья" and "Язык" has already been taken.'
			],
            [['wiki_article_id'], 'exist', 'skipOnError' => true, 'targetClass' => WikiArticle::className(), 'targetAttribute' => ['wiki_article_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wiki_article_id' => Yii::t('app', 'Модуль'),
            'language' => Yii::t('app', 'Язык'),
            'content' => Yii::t('app', 'Статья'),
        ];
    }

}
