<?php

namespace common\modules\wiki\models;

use Yii;

/**
 * This is the model class for table "wiki_article".
 *
 * @property integer $id
 * @property integer $wiki_category_id
 * @property string $name
 */
class WikiArticle extends \yii\db\ActiveRecord
{
	public $content;

	public $category_name;
	public $languages;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wiki_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wiki_category_id', 'name'], 'required'],
            [['wiki_category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['wiki_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => WikiCategory::className(), 'targetAttribute' => ['wiki_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wiki_category_id' => Yii::t('app', 'Категория'),
            'category_name' => Yii::t('app', 'Категория'),
            'name' => Yii::t('app', 'Название'),
        ];
    }

	/**
	 * @param $model WikiArticleLanguage
	 * @param $post []
	 */
    public function saveLanguage($model, $post)
	{
		if($model->isNewRecord){
			$model->wiki_article_id = $this->id;
		}

		$model->load($post);
		$model->save();
	}

    public static function findOneFull($id, $language = null){
		$query = self::find()->where([self::tableName().'.id'=>$id]);

		$query->select([self::tableName().'.*', 'l.content content']);
		$query->innerJoin(WikiArticleLanguage::tableName().' l', 'l.wiki_article_id='.self::tableName().'.id');

		if($language){
			$query->andWhere(['l.language' => $language]);
		}

		return $query->one();
	}

}
