<?php

namespace common\modules\wiki\models\search;

use common\modules\wiki\models\WikiArticleLanguage;
use common\modules\wiki\models\WikiCategory;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\wiki\models\WikiArticle;

/**
 * ArticleSearch represents the model behind the search form about `common\modules\wiki\models\WikiArticle`.
 */
class ArticleSearch extends WikiArticle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'wiki_category_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WikiArticle::find();


		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		$select = [
			self::tableName().'.*',
			'c.name category_name',
			'GROUP_CONCAT(l.`language`) languages',
		];

		$on = 'l.wiki_article_id = '.self::tableName().'.id';
		if($this->id){
			$select[] = 'l.content content';
			$on .= ' AND l.language=\''.Yii::$app->language.'\'';
		}

//		var_dump($select); die;

		$query->select($select);
		$query->innerJoin(WikiCategory::tableName().' c',  'c.id = '.self::tableName().'.wiki_category_id');
		$query->leftJoin(WikiArticleLanguage::tableName().' l',  $on);
		$query->groupBy(self::tableName().'.id');

		if(!$this->wiki_category_id){
			$where = WikiCategory::getWhere();
			if($where){
				$query->where($where);
			}
		}

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
			self::tableName().'.id' => $this->id,
            'wiki_category_id' => $this->wiki_category_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
