<?php

namespace common\modules\wiki\widgets;

use common\modules\wiki\models\WikiCategory;
use yii\base\Widget;

class CategoryWidget extends Widget
{
	public $title = 'Разделы';

	public $module;
	public $element;
	public $href = '#';
	public $active_id;

	public function init(){
		parent::init();
	}
	
	public function run(){
		/** @var WikiCategory[] $category */
		$category = WikiCategory::getCategoryForModule($this->module, $this->element);
		$content = '<div class="panel panel-default">';
		$content .= '<div class="panel-heading">'.$this->title.'</div>';
		$content .= '<div class="list-group">';

		foreach($category as $item){
			$class = ($this->active_id==$item->id) ? 'active disabled' : '';
			$href = strtr($this->href, ['{{id}}'=>$item->id]);

			$content .= '<a href="'.$href.'" class="list-group-item '.$class.'">';
			$content .= $item->name;
//			$content .= '<span class="badge">25</span>';
			$content .= '</a>';
		}

		$content .= '</div></div>';

		return $content;
	}
}
?>
