<?php

namespace common\modules\wiki\widgets;

use common\modules\wiki\models\search\ArticleSearch;
use common\modules\wiki\models\WikiArticle;
use common\modules\wiki\models\WikiCategory;
use yii\base\Widget;

class ArticleWidget extends Widget
{
	public $module;
	public $element;
	public $href;

	public $active_id;
	public $category_id;

	public function init(){
		parent::init();
	}
	
	public function run(){
		/** @var WikiArticle[] $article */
		$query = new ArticleSearch();
		$article = $query->search([
			'ArticleSearch' => [
				'wiki_category_id'=>$this->category_id,
				'id' => \Yii::$app->request->get('article_id')
			]
		])->query->all();

		$text = '';
		$languages = '';
		$content = '<article class="list-group">';

		foreach($article as $item){
			$languages = $item->languages;
			$text = ($item->content) ? $item->content : '';
			$class = ($this->active_id==$item->id) ? 'list-group-item-info' : '';
			$href = strtr($this->href, ['{{id}}'=>$item->id]);

			$content .= '<a href="'.$href.'" class="list-group-item '.$class.'">';
			$content .= $item->name;
			$content .= '</a>';
		}

		$content .= '</article>';

		if($text){
			$content .= '<div class="list-group">';
			$content .= $text;
			$content .= '</div>';
		}

		if(\Yii::$app->request->get('article_id') && !$text){
			$content .= '<div class="list-group">';
			$content .= $languages;
			$content .= '</div>';
		}

		return $content;
	}
}
?>
