<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiCategory */
?>
<div class="wiki-category-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'module',
            'element',
            'name',
        ],
    ]) ?>

</div>
