<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiCategory */

?>
<div class="wiki-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
