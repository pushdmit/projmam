<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */

$prefix = '/'.Yii::$app->request->get('module').'/'.Yii::$app->request->get('element_id');
?>

<div class="btn-group" role="group" aria-label="..." style="padding:0 0 30px">
    <?= Html::a(
    	Yii::t('wiki', 'Wiki'),
		[$prefix.'/wiki'],
		['class' => 'btn btn-default']
    ) ?>

	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('wiki', 'Разделы')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('wiki', 'Разделы'), [$prefix.'/wiki/category']) ?>
			</li>
			<li>
				<?= Html::a(
					Yii::t('wiki', 'Добавить раздел'),
					[$prefix.'/wiki/category/create'],
					['role' => 'modal-remote']
				) ?>
			</li>
		</ul>
	</div>

	<?= Html::a(
		Yii::t('wiki', 'Виджеты'),
		[$prefix.'/wiki/default/widget'],
		['class' => 'btn btn-default']
	) ?>

	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('frontend', 'Управление')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('finance', 'Manager'), [$prefix.'/manager']) ?>
			</li>
		</ul>
	</div>
</div>
