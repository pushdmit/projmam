<?php

use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use common\modules\wiki\widgets;

$this->title = Yii::t('wiki', 'Виджеты');

\frontend\assets\CrudAsset::register($this);
$prefix = '/'.Yii::$app->request->get('module').'/'.Yii::$app->request->get('element_id');
?>

<?= $this->render('../default/_menu') ?>

<div class="wiki-default-index">
	<?php Pjax::begin(['id'=>'crud-datatable-pjax'])?>
	<div class="row">
		<div class="col-md-3">
			<?= widgets\CategoryWidget::widget([
				'module' => 'project',
				'element' => 'project3',
				'href' => $prefix.'/wiki/default/widget?id={{id}}',
				'active_id' => Yii::$app->request->get('id'),
			])?>
		</div>

		<div class="col-md-9">
			<?= widgets\ArticleWidget::widget([
				'module' => Yii::$app->request->get('module'),
				'element' => Yii::$app->request->get('element_id'),
				'category_id' => Yii::$app->request->get('id'),
				'href' => $prefix.'/wiki/default/widget?id='.Yii::$app->request->get('id').'&article_id={{id}}',
				'active_id' => Yii::$app->request->get('article_id'),
			])?>
		</div>
	</div>
	<?php Pjax::end() ?>
</div>

<?php Modal::begin([
	"id"=>"ajaxCrudModal",
	"footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
