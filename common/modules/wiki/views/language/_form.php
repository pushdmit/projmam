<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiArticleLanguage */
/* @var $form yii\widgets\ActiveForm */

$is_form = true;
if(isset($form)){
	$is_form = false;
}

$this->registerJs("CKEDITOR.plugins.addExternal('mathjax', '/js/mathjax/plugin.js', '');");
$this->registerJs("CKEDITOR.config.mathJaxLib = '//cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML';");
//$this->registerJs("CKEDITOR.ui.add(\"mathjax\", {
//                            label: \"MathJax\",
//                            icon: this.path + \"icons/mathjax.png\"
//                        });");
//$this->registerJs("CKEDITOR.replace( 'editor1', {
//	extraPlugins: 'mathjax',
//	mathJaxLib: 'http://cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML',
//	height: 320
//} );
//
//if ( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) {
//	document.getElementById( 'ie8-warning' ).className = 'tip alert';
//}");
//var_dump(Yii::$app->request->get('id')); die;
?>

<div class="wiki-article-language-form">

	<? if($is_form){ ?>
    	<?php $form = ActiveForm::begin(); ?>

    	<?= $form->field($model, 'wiki_article_id')->textInput() ?>
	<? } ?>

    <?= $form->field($model, 'language')->dropDownList(Yii::$app->languagepicker->languages) ?>

	<?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), [
		'options' => [
			'rows' => 6,
		],
		'preset' => 'basic',
		'clientOptions' => [
//			'toolbarGroups' => [
//				['name' => 'clipboard', 'groups' => ['mode', 'undo', 'selection', 'clipboard', 'doctools']],
//				['name' => 'editing', 'groups' => ['tools', 'about']],
//				['name' => 'paragraph', 'groups' => ['templates', 'list', 'indent', 'align']],
//				['name' => 'insert'],
//				'/',
//				['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
//				['name' => 'colors'],
//				['name' => 'links'],
//				['name' => 'others'],
//			],
			'extraPlugins' => 'codesnippet,filebrowser',//,mathjax
			'filebrowserBrowseUrl' => '/kcfinder/browse.php?opener=ckeditor&type=images',
			'filebrowserUploadUrl' => '/project/'.Yii::$app->request->get('id').'/wiki/default/upload',
		],
	]) ?>

	<? if($is_form){ ?>
		<?php if(!Yii::$app->request->isAjax){ ?>
			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('finance', 'Create') : Yii::t('finance', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
		<?php } ?>
    	<?php ActiveForm::end(); ?>
	<? } ?>
    
</div>

<script type="text/javascript" src="http://cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML"></script>
