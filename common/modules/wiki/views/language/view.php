<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiArticleLanguage */
?>
<div class="wiki-article-language-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'wiki_article_id',
            'language',
            'content:ntext',
        ],
    ]) ?>

</div>
