<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiArticleLanguage */
?>
<div class="wiki-article-language-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
