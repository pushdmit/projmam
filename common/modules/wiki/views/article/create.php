<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiArticle */
/* @var $modelLanguage common\modules\wiki\models\WikiArticleLanguage */

?>
<div class="wiki-article-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelLanguage' => $modelLanguage,
    ]) ?>
</div>
