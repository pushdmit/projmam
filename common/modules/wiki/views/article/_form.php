<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\wiki\models;

/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiArticle */
/* @var $modelLanguage common\modules\wiki\models\WikiArticleLanguage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wiki-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wiki_category_id')->dropDownList(models\WikiCategory::getListMap()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $this->render('../language/_form', ['form' => $form, 'model' => $modelLanguage]) ?>

	<?php if(!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
