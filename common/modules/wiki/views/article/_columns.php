<?php
use yii\helpers\Url;

$prefix = '/'.Yii::$app->request->get('module').'/'.Yii::$app->request->get('element_id');

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
	[
		'attribute' => 'languages',
		'label' => 'Languages',
		'format' => 'raw',
		'value' => function($data) use($prefix) {
			$links = [];
			$languages = Yii::$app->languagepicker->languages;
			$pagesLanguage = explode(',', $data->languages);
			//var_dump($pagesLanguage); die;

			foreach($languages as $key => $value){
				$link = $prefix.'/wiki/article/update?id='.$data->id.'&language='.$key;
				$option = ['class'=>'color-red', 'style'=>'color:red'];
				if(in_array($key, $pagesLanguage)){
					$option = [];
				}
				$links[] =  \yii\bootstrap\Html::a($value, $link, $option+['role'=>'modal-remote']);
			}
			return implode(', ', $links);
		},
	],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) use($prefix) {
                return $prefix.Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
		'deleteOptions' => [
			'role' => 'modal-remote', 'title' => 'Delete',
			'data-confirm' => false, 'data-method' => false,
			'data-request-method' => 'post',
			'data-toggle' => 'tooltip',
			'data-confirm-title' => 'Are you sure?',
			'data-confirm-message' => 'Are you sure want to delete this item'
		],
    ],

];   