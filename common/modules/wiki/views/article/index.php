<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use frontend\assets\CrudAsset;
use common\widgets\ButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\wiki\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('wiki', 'Wiki Articles');
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<?= $this->render('../default/_menu') ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="wiki-article-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a(
                    	'<i class="glyphicon glyphicon-plus"></i>',
						['/'.Yii::$app->request->get('module').'/'.Yii::$app->request->get('element_id').'/wiki/article/create'],
                    	['role'=>'modal-remote','title'=> 'Create new Wiki Articles','class'=>'btn btn-default']
					).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid'])
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Wiki Articles listing',
                'before'=>'',
                'after'=>ButtonWidget::widget([
                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
                        ["bulk-delete"] ,
                        [
                            "class"=>"btn btn-danger btn-xs",
                            'role'=>'modal-remote-bulk',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Are you sure?',
                            'data-confirm-message'=>'Are you sure want to delete this item'
                        ]),
                ]).
                '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
