<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\wiki\models\WikiArticle */
?>
<div class="wiki-article-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'wiki_category_id',
            'name',
        ],
    ]) ?>

	<div>
		<?php \dosamigos\ckeditor\CKEditorInline::begin([
			'preset' => 'basic',
				'clientOptions' => [
				'extraPlugins' => 'codesnippet,lineutils,dialog,clipboard',//,mathjax
			],
		]);?>
			<?=$model->content?>
		<?php \dosamigos\ckeditor\CKEditorInline::end();?>
	</div>

</div>

<link href="http://projman.loc/assets/73d04a0b/contents.css?t=GB8B" rel="stylesheet">
