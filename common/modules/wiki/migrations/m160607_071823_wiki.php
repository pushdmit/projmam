<?php

use yii\db\Migration;

class m160607_071823_wiki extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

	    $this->createTable('{{%wiki_category}}', [
		    'id' => $this->primaryKey(),
			'module' => $this->string()->notNull()->comment('Модуль'),
			'element' => $this->string()->notNull()->comment('Элемент'),
		    'name' => $this->string()->notNull()->comment('Название'),
	    ], $tableOptions);

        $this->createTable('{{%wiki_article}}', [
            'id' => $this->primaryKey(),
			'wiki_category_id' => $this->integer()->notNull()->comment('Категория'),
	        'name' => $this->string()->notNull()->comment('Название'),
        ], $tableOptions);

        $this->createTable('{{%wiki_article_language}}', [
            'id' => $this->primaryKey(),
	        'wiki_article_id' => $this->integer()->notNull()->comment('Модуль'),
	        'language' => $this->string()->notNull()->comment('Язык'),
	        'content' => $this->text()->notNull()->comment('Статья'),
        ], $tableOptions);

		$this->addForeignKey('{{%wiki_article_fk1}}', '{{%wiki_article}}', 'wiki_category_id', '{{%wiki_category}}', 'id');

		$this->addForeignKey('{{%wiki_article_language_fk1}}', '{{%wiki_article_language}}', 'wiki_article_id', '{{%wiki_article}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%wiki_article_language}}');
        $this->dropTable('{{%wiki_article}}');
        $this->dropTable('{{%wiki_category}}');
    }
}
