<?php
namespace common\widgets;

use yii\base\Widget;

class ButtonWidget extends Widget{

	public $buttons;
	
	public function init(){
		parent::init();
	}
	
	public function run(){
		$content = '<div class="pull-left">'.
                   '<span class="glyphicon glyphicon-arrow-right"></span> Выброное '.
                   $this->buttons.
                   '</div>';
		return $content;
	}
}
?>
