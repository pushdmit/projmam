<?php
namespace common\widgets;

use common\models\TaskTime;
use yii\base\Widget;

class TimeWidget extends Widget{

	public $title = 'Трудозатраты (последние 7 дней)';
	public $date_start;

	public function init(){
		parent::init();
	}
	
	public function run(){
		/** @var TaskTime[] $category */
		$category = TaskTime::getTimePeriod($this->date_start);
		$content = '<div class="panel panel-default">';
		$content .= '<div class="panel-heading" style="text-align: center">'.$this->title.'</div>';
		$content .= '<table class="table" style="text-align: center">';
		$content .= '<tr><th style="text-align: center">Дата</th><th style="text-align: center">Время</th></tr>';

		foreach($category as $item){
			$content .= '<tr>';
			$content .= '<td>'.\Yii::$app->formatter->asDate($item->date_create).'</td>';
			$content .= '<td>'.$item->getQuantityTime().'</td>';
			$content .= '</tr>';
		}

		$content .= '</table></div>';

		return $content;
	}
}
?>
