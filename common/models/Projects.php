<?php

namespace common\models;

use common\models\search\TasksStatusSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $user_id
 * @property integer $status
 * @property string $is_active
 * @property string $is_public
 *
 * @property User $user
 */
class Projects extends \yii\db\ActiveRecord
{
	
	public static $aliasLast = '';
	/** @var  Projects */
	public static $projectCurrent;
	public static $users = [];
	public static $tasks = [];

	public function setStatic(){
		self::$aliasLast = $this->alias;
		self::$projectCurrent = $this;
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'projects';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'alias', 'user_id'], 'required'],
			[['user_id', 'status'], 'integer'],
			[['name', 'alias'], 'string', 'max' => 255],
			[['is_active', 'is_public'], 'string', 'max' => 1],
			[['alias'], 'unique'],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('frontend', 'ID'),
			'name' => Yii::t('frontend', 'Name'),
			'alias' => Yii::t('frontend', 'Alias'),
			'user_id' => Yii::t('frontend', 'User ID'),
			'status' => Yii::t('frontend', 'Status'),
			'is_active' => Yii::t('frontend', 'Is Active'),
			'is_public' => Yii::t('frontend', 'Is Public'),
		];
	}

	public static function findAliasOrId($alias)
	{
		$project = null;

		if(is_numeric($alias)){
			$project = self::findOne($alias);
		} else {
			$project = self::find()->where(['alias' => $alias])->one();
		}

		if(!$project){
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		if(Yii::$app->getUser()->getId()!=$project->user_id && !isset($project->getUser()[Yii::$app->getUser()->getId()])){
			throw new HttpException(403, 'You are not allowed to perform this action.');
		}

		return $project;
	}

	public function getUser()
	{
		if(!self::$users)
		{
			self::$users = User::find()
				->select(User::tableName().'.*')
				->where(['project_id' => $this->id])
				->innerJoin(ProjectUsers::tableName().' pu', 'pu.user_id = '.User::tableName().'.id')
				->indexBy('id')
				->all();
		}

//		var_dump(self::$users); die;

		return self::$users;
	}

	public function getTasks($is_active = false)
	{
		if(!self::$tasks)
		{
			$tasks = Tasks::find()->where([Tasks::tableName().'.project_id' => $this->id]);

			if($is_active){
				$tasks->leftJoin(TaskStatus::tableName().' s', 's.id = '.Tasks::tableName().'.task_status_id');
				$tasks->andWhere(['s.is_closed'=>0]);
			}

			self::$tasks = $tasks->all();
		}

		return self::$tasks;
	}

	public function getUserMap()
	{
		return ArrayHelper::map($this->getUser(), 'id', 'username');
	}

	public function createDefaultTaskStatus()
	{
		foreach(TaskStatus::$default as $item){
			$status = new TaskStatus();
			$status->load($item, '');
			$status->project_id = $this->id;
			$status->save();
		}
	}

	public function createDefaultTaskTracker()
	{
		foreach(TaskTrackers::$default as $item){
			$status = new TaskTrackers();
			$status->load($item, '');
			$status->project_id = $this->id;
			$status->save();
		}
	}

	public function createNewUser()
	{
		$user = new ProjectUsers();
		$user->user_id = Yii::$app->user->id;
		$user->project_id = $this->id;
		$user->save();
	}

	public function getProjectTasksMap($is_active = false)
	{
		return ArrayHelper::map($this->getTasks($is_active), 'id', 'name');
	}

	public static function getAlias()
	{
		return self::$aliasLast;
	}

	public static function getProject()
	{
		return self::$projectCurrent;
	}
}