<?php

namespace common\models;

use common\helper\StatusHelper;
use Yii;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $project_id
 * @property integer $user_id
 * @property integer $task_trackers_id
 * @property integer $user_executor_id
 * @property integer $task_status_id
 * @property integer $parent_task_id
 * @property integer $is_delete
 * @property string $priority
 * @property string $readiness
 * @property string $date_start
 * @property string $date_end
 * @property string $time_evaluation
 * @property string $time_elapsed
 *
 * @property TaskStatus $taskStatus
 * @property Projects $project
 * @property User $user
 * @property User $user0
 */
class Tasks extends \yii\db\ActiveRecord
{
//	public $priority = 5;

	public $task_status_name;
	public $user_name;
	public $user_executor_name;

	public $pks;

	public static $event_list = [
		'self::UpdateDateParent'
	];

	public static $readiness_list = [
		0 => '0%',
		10 => '10%',
		20 => '20%',
		30 => '30%',
		40 => '40%',
		50 => '50%',
		60 => '60%',
		70 => '70%',
		80 => '80%',
		90 => '90%',
		100 => '100%',
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'createdAtAttribute' => 'date_create',
				'updatedAtAttribute' => null,
				'value' => function($event){
					return date("Y-m-d H:i:s");
				},
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'project_id', 'user_id', 'user_executor_id'], 'required'],
            [['description'], 'string'],
            [['project_id', 'user_id', 'task_trackers_id', 'user_executor_id', 'task_status_id', 'priority', 'parent_task_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['is_delete'], 'integer'],
            [['time_evaluation', 'time_elapsed'], 'number'],
            [['readiness'], 'integer', 'min' => 0, 'max' => 100],
            [['task_trackers_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskTrackers::className(), 'targetAttribute' => ['task_trackers_id' => 'id']],
            [['task_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskStatus::className(), 'targetAttribute' => ['task_status_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
	        [['date_start', 'date_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'name' => Yii::t('frontend', 'Название'),
            'description' => Yii::t('frontend', 'Описание'),
            'project_id' => Yii::t('frontend', 'Проект'),
            'user_id' => Yii::t('frontend', 'Создатель'),
            'task_trackers_id' => Yii::t('frontend', 'Трекер'),
            'user_executor_id' => Yii::t('frontend', 'Исполнитель'),
            'task_status_id' => Yii::t('frontend', 'Статус'),
            'is_delete' => Yii::t('frontend', 'Is Delete'),
            'priority' => Yii::t('frontend', 'Приоритет'),
            'readiness' => Yii::t('frontend', 'Готовность'),
            'parent_task_id' => Yii::t('frontend', 'Родительская задача'),
            'date_start' => Yii::t('frontend', 'Дата начала'),
            'date_end' => Yii::t('frontend', 'Дата завершения'),
            'time_evaluation' => Yii::t('frontend', 'Оценка ч.'),
            'time_elapsed' => Yii::t('frontend', 'Затраты ч.'),
        ];
    }

	public function save($runValidation = true, $attributeNames = null)
	{
		$field_old = $this->getOldAttributes();
		$field_new = $this->getAttributes();
		$fields = [];

		if($this->id && $field_old != $field_new){
			foreach($field_old as $key => $value){
				if($value != $field_new[$key]){
					$fields[$key] = [
						'name' => $this->attributeLabels()[$key],
						'old' => $value,
						'new' => $field_new[$key],
					];
				}
			}

			if($fields){
				$change = new TaskChanges();
				$change->task_id = $this->id;
				$change->user_id = Yii::$app->user->id;
				$change->fields = json_encode($fields);
				$change->save();
				
				if(isset($fields['task_status_id'])){
					$change = new TaskChangeStatus();
					$change->task_id = $this->id;
					$change->user_id = Yii::$app->user->id;
					$change->task_status_id_old = $fields['task_status_id']['old'];
					$change->task_status_id_new = $fields['task_status_id']['new'];
					$change->save();
				}
			}
		}

//		$result = 1;
		$result = parent::save($runValidation, $attributeNames);

		if($result && $this->parent_task_id){
			self::UpdateDateParent($this);
		}

		return $result;
	}

	public static function UpdateDateParent($current_task){
		$task = Tasks::findOne($current_task->parent_task_id);
		$task->date_start = Tasks::find()->where(['parent_task_id'=>$task->id])->min('date_start');
		$task->date_end = Tasks::find()->where(['parent_task_id'=>$task->id])->max('date_end');

		$avg = Tasks::find()->where(['parent_task_id'=>$task->id])->average('readiness');
		$avg = $avg-($avg%10);
		$task->readiness = $avg;

//		$task->time_evaluation = Tasks::find()->where(['parent_task_id'=>$task->id])->sum('time_evaluation');
//		$task->time_elapsed = Tasks::find()->where(['parent_task_id'=>$task->id])->sum('time_elapsed');
		$task->save();
//		var_dump($this->date_start, $this->date_end); die;
	}

	public function addTaskTime()
	{
		$request = Yii::$app->request;
		$time = new TaskTime();

		$time->task_id = $this->id;
		$time->user_id = Yii::$app->getUser()->id;
		$time->load($request->post());

		$time->save();
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskStatus()
    {
        return $this->hasOne(TaskStatus::className(), ['id' => 'task_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
