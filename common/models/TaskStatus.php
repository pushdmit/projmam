<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task_status".
 *
 * @property integer $id
 * @property string $name
 * @property integer $project_id
 * @property integer $is_active
 * @property integer $is_closed
 *
 * @property Projects $project
 * @property Tasks[] $tasks
 */
class TaskStatus extends \yii\db\ActiveRecord
{

	public static $default = [
		['name' => 'Новая', 'is_closed' => 0, 'is_active' => 1],
		['name' => 'В работе', 'is_closed' => 0, 'is_active' => 1],
		['name' => 'Решена', 'is_closed' => 0, 'is_active' => 1],
		['name' => 'Закрыта', 'is_closed' => 1, 'is_active' => 1],
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'project_id'], 'required'],
            [['project_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['is_active', 'is_closed'], 'integer'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'name' => Yii::t('frontend', 'Name'),
            'project_id' => Yii::t('frontend', 'Project ID'),
            'is_active' => Yii::t('frontend', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['task_status_id' => 'id']);
    }

	public static function getProjectStatus($project_id, $is_map = false)
	{
		$statuses = self::find()->where(['project_id' => $project_id, 'is_active' => 1])->all();

		if($is_map){
			return ArrayHelper::map($statuses, 'id', 'name');
		}

		return $statuses;
	}
}
