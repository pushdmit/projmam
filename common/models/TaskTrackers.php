<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task_trackers".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $name
 * @property integer $sort
 */
class TaskTrackers extends \yii\db\ActiveRecord
{
	public static $default = [
		['name' => 'Разработка', 'sort' => 1],
		['name' => 'Ошибка', 'sort' => 2],
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_trackers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'name'], 'required'],
            [['project_id', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'project_id' => Yii::t('app', 'Проект'),
            'name' => Yii::t('app', 'Название'),
            'sort' => Yii::t('app', 'Позиция'),
        ];
    }


	public static function getForProject($project_id, $is_map = false)
	{
		$statuses = self::find()->where(['project_id' => $project_id])->all();

		if($is_map){
			return ArrayHelper::map($statuses, 'id', 'name');
		}

		return $statuses;
	}

}
