<?php

//namespace common\models\mongodb;

use Yii;

/**
 * This is the model class for collection "address".
 *
 * @property \MongoId|string $_id
 * @property mixed $building
 * @property mixed $pincode
 * @property mixed $city
 * @property mixed $state
 * @property mixed $user_id
 */
class Address// extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['mydatabase', 'address'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'building',
            'pincode',
            'city',
            'state',
            'user_id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['building', 'pincode', 'city', 'state', 'user_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'building' => Yii::t('app', 'Building'),
            'pincode' => Yii::t('app', 'Pincode'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'user_id' => Yii::t('app', 'User Id'),
        ];
    }
}
