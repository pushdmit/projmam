<?php

//namespace common\models\mongodb;

use Yii;

/**
 * This is the model class for collection "customer".
 *
 * @property \MongoId|string $_id
 * @property mixed $name
 * @property mixed $status
 * @property mixed $address
 */
class Customer// extends \yii\mongodb\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function collectionName()
	{
		return ['mydatabase', 'customer'];
	}

	/**
	 * @inheritdoc
	 */
	public function attributes()
	{
		return [
			'_id',
			'name',
			'status',
			'address',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'status', 'address'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'_id' => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
			'status' => Yii::t('app', 'Status'),
			'address' => Yii::t('app', 'Addres'),
		];
	}
}