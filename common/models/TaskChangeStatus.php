<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task_change_status".
 *
 * @property integer $task_id
 * @property integer $user_id
 * @property integer $task_status_id_new
 * @property integer $task_status_id_old
 * @property string $date_create
 */
class TaskChangeStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_change_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'user_id', 'task_status_id_new', 'task_status_id_old'], 'required'],
            [['task_id', 'task_status_id_new', 'task_status_id_old'], 'integer'],
            [['date_create'], 'safe'],
	        [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasks::className(), 'targetAttribute' => ['task_id' => 'id']],
	        [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('app', 'Task ID'),
	        'user_id' => Yii::t('app', 'User ID'),
            'task_status_id_new' => Yii::t('app', 'Field New'),
            'task_status_id_old' => Yii::t('app', 'Field Old'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }
}
