<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "finance".
 *
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 */
class Finance extends \yii\db\ActiveRecord
{
	public $user_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'user_id' => Yii::t('app', 'Создатель'),
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
	    if($this->isNewRecord){
		    $this->user_id = Yii::$app->getUser()->id;
	    }

	    return parent::save($runValidation, $attributeNames);
    }
}
