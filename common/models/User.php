<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_user".
 *
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 */
class User extends \amnah\yii2\user\models\User
{
	public static $users;

	public $user_name;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return "{{%user}}";
	}

	public function getUserName()
	{
		$name = '';
		if($this->first_name){
			if($name) $name .= ' ';
			$name .= $this->first_name;
		}
		if($this->last_name){
			if($name) $name .= ' ';
			$name .= $this->last_name;
		}

		if(!$name){
			$name = $this->email;
		}

		return $name;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		// set initial rules
		$rules = [
			// general email and username rules
			[['email', 'username'], 'string', 'max' => 255],
			[['email', 'username'], 'unique'],
			[['email', 'username'], 'filter', 'filter' => 'trim'],
			[['email'], 'email'],
			[['username'], 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => Yii::t('user', '{attribute} can contain only letters, numbers, and "_"')],

			// password rules
			[['newPassword'], 'string', 'min' => 3],
			[['newPassword'], 'filter', 'filter' => 'trim'],
			[['newPassword'], 'required', 'on' => ['register', 'reset']],
			[['newPasswordConfirm'], 'required', 'on' => ['reset']],
			[['newPasswordConfirm'], 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('user', 'Passwords do not match')],

			// account page
			[['currentPassword'], 'validateCurrentPassword', 'on' => ['account']],

			// admin crud rules
			[['role_id', 'status'], 'required', 'on' => ['admin']],
			[['role_id', 'status'], 'integer', 'on' => ['admin']],
			[['banned_at'], 'integer', 'on' => ['admin']],
			[['banned_reason'], 'string', 'max' => 255, 'on' => 'admin'],

			[['first_name', 'last_name'], 'required', 'on' => ['admin']],
			[['first_name', 'last_name', 'middle_name'], 'string', 'max' => 32],
		];

		// add required for currentPassword on account page
		// only if $this->password is set (might be null from a social login)
		if ($this->password) {
			$rules[] = [['currentPassword'], 'required', 'on' => ['account']];
		}

		// add required rules for email/username depending on module properties
		$requireFields = ["requireEmail", "requireUsername"];
		foreach ($requireFields as $requireField) {
			if ($this->module->$requireField) {
				$attribute = strtolower(substr($requireField, 7)); // "email" or "username"
				$rules[] = [$attribute, "required"];
			}
		}

		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('user', 'ID'),
			'role_id' => Yii::t('user', 'Role ID'),
			'status' => Yii::t('user', 'Status'),
			'email' => Yii::t('user', 'Email'),

			'username' => Yii::t('user', 'Username'),
			'first_name' => Yii::t('user', 'Фамилия'),
			'last_name' => Yii::t('user', 'Имя'),
			'middle_name' => Yii::t('user', 'Отчество'),

			'password' => Yii::t('user', 'Password'),
			'auth_key' => Yii::t('user', 'Auth Key'),
			'access_token' => Yii::t('user', 'Access Token'),
			'logged_in_ip' => Yii::t('user', 'Logged In Ip'),
			'logged_in_at' => Yii::t('user', 'Logged In At'),
			'created_ip' => Yii::t('user', 'Created Ip'),
			'created_at' => Yii::t('user', 'Created At'),
			'updated_at' => Yii::t('user', 'Updated At'),
			'banned_at' => Yii::t('user', 'Banned At'),
			'banned_reason' => Yii::t('user', 'Banned Reason'),

			// virtual attributes set above
			'currentPassword' => Yii::t('user', 'Current Password'),
			'newPassword' => $this->isNewRecord ? Yii::t('user', 'Password') : Yii::t('user', 'New Password'),
			'newPasswordConfirm' => Yii::t('user', 'New Password Confirm'),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'value' => function ($event) {
					return gmdate("Y-m-d H:i:s");
				},
			],
		];
	}

	public function getName($format = 'fn ln mn'){
		$tr = [
			'ln' => $this->last_name,
			'fn' => $this->first_name,
			'mn' => $this->middle_name,
		];
		return trim(strtr($format, $tr));
	}


	public function can($permissionName, $params = [], $allowCaching = true)
	{
		$auth = Yii::$app->getAuthManager();
		if($auth) {
			return parent::can($permissionName, $params, $allowCaching);
		}

		$user = $this->getIdentity();
		return $user ? $user->can($permissionName) : false;
	}

	public static function getMap(){
		$maps = self::find()->select('id', 'IFNULL(CONCAT(last_name, \' \', first_name), email) user_name, ')
			->orderBy('last_name')->asArray()->all();
		return [''=>'Пользователь']+ArrayHelper::map($maps, 'id', 'user_name');
	}

	public function isOnline(){
		return $this->logged_in_at &&
		Yii::$app->formatter->asDate($this->logged_in_at, 'php:Y-m-d H-i-s')>=date('Y-m-d H-i-s', time()-60*10);
	}

	public static function getUser()
	{
		if(!self::$users)
		{
			$select = [
				self::tableName().'.*',
				'IFNULL(CONCAT(last_name, \' \', first_name), email) user_name',
			];

			self::$users = User::find()
				->select($select)
				->all();
		}

		return self::$users;
	}

	public static function getUserMap()
	{
		return ArrayHelper::map(self::getUser(), 'id', 'user_name');
	}
}
