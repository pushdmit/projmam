<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task_time".
 *
 * @property integer $task_id
 * @property integer $user_id
 * @property string $quantity
 * @property string $date_create
 *
 * @property User $user
 * @property Tasks $task
 */
class TaskTime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'user_id', 'quantity'], 'required'],
            [['task_id', 'user_id'], 'integer'],
            [['quantity'], 'number'],
            [['date_create'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasks::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('app', 'Task ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
	    if($this->validate()){
		    $task = Tasks::findOne($this->task_id);
		    $sum = self::find()->where(['task_id'=>$this->task_id])->sum('quantity');
		    $task->time_elapsed = $sum+$this->quantity;
		    $task->save();
	    }

	    return parent::save($runValidation, $attributeNames);
    }

	public static function getTimePeriod($date_start = null, $period = 'P1D')
	{
		if(!$date_start){
			$date_start = date('Y-m-d', time()-60*60*24*6);
		}

		$select = [
			'user_id',
			'SUM(quantity) quantity',
			'DATE(date_create) date_create',
		];

		$query = self::find();
		$query->select($select);
		$query->andFilterWhere([
			'user_id' => Yii::$app->getUser()->getId(),
		]);
		$query->andFilterWhere(['>', 'date_create', $date_start]);
		$query->groupBy('DATE(date_create)');

		return $query->all();
	}

	public function getQuantityTime()
	{
		$o = (($this->quantity*100)%100)/100;
		$h = $this->quantity-$o;
		$m = round(60*$o);

		return $h.'ч. '.$m.'м.';
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'task_id']);
    }
}
