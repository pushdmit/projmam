<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task_changes".
 *
 * @property integer $task_id
 * @property integer $user_id
 * @property string $fields
 * @property string $date_create
 *
 * @property Tasks $task
 * @property Tasks $task0
 */
class TaskChanges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_changes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'fields', 'user_id'], 'required'],
            [['task_id'], 'integer'],
            [['fields'], 'string'],
            [['date_create'], 'safe'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasks::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => Yii::t('app', 'Task ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'fields' => Yii::t('app', 'Field'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask0()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'task_id']);
    }
}
