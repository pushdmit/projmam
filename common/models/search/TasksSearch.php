<?php

namespace common\models\search;

use common\models\TaskStatus;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tasks;

/**
 * TasksSearch represents the model behind the search form about `common\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'user_id', 'task_trackers_id', 'user_executor_id', 'task_status_id', 'priority', 'readiness'], 'integer'],
            [['name', 'description', 'is_delete'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tasks::find();

	    $select = [
		    self::tableName().'.*',
		    's.name task_status_name',
		    'IFNULL(CONCAT(u.last_name, \' \', u.first_name), u.email) user_name',
		    'IFNULL(CONCAT(ue.last_name, \' \', ue.first_name), ue.email) user_executor_name',
	    ];

	    $query->select($select);
	    $query->innerJoin(TaskStatus::tableName().' s', 's.id = '.self::tableName().'.task_status_id');
	    $query->innerJoin(User::tableName().' u', 'u.id = '.self::tableName().'.user_id');
	    $query->innerJoin(User::tableName().' ue', 'ue.id = '.self::tableName().'.user_executor_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
	        self::tableName().'.project_id' => $this->project_id,
            'user_id' => $this->user_id,
            'task_trackers_id' => $this->task_trackers_id,
            'user_executor_id' => $this->user_executor_id,
            'task_status_id' => $this->task_status_id,
            'priority' => $this->priority,
        ]);

	    if(!$this->task_status_id){
		    $query->andWhere('s.is_closed = 0');
	    }

	    if(!Yii::$app->request->get('sort')){
		    $query->orderBy(self::tableName().'.id DESC');
	    }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'is_delete', $this->is_delete]);

        return $dataProvider;
    }
}
