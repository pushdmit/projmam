<?php
return [
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'language' => 'ru',
	'sourceLanguage' => 'ru-RU',
	'components' => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				// your rules go here
			],
		],
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
			'cache' => 'cache',
		],
		'user' => [
			'class' => 'amnah\yii2\user\components\User',
		],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\DbMessageSource',
					'db' => 'db',
					'sourceLanguage' => 'ru-RU', // Developer language
					'sourceMessageTable' => '{{%language_source}}',
					'messageTable' => '{{%language_translate}}',
//					'cachingDuration' => 86400,
					'enableCaching' => false,
				],
			],
		],
		'translatemanager' => [
			'class' => 'lajax\translatemanager\Component'
		],
		'db' => [
			'enableSchemaCache' => true,
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => false,
			'messageConfig' => [
				'from' => ['noreply@moy-dom.online' => 'Admin'],
				'charset' => 'UTF-8',
			],
			'transport' => [
				'class' => 'Swift_SendmailTransport',
				'command' => '/usr/sbin/sendmail -t -i',
			],
		],
		'redis' => [
			'class' => 'heyanlong\redis\Connection',
			'hostname' => 'localhost',
//			'port' => 7000,
			'master' => [
				'localhost:7000',
				'localhost:7001',
				'localhost:7002',
			],
			'database' => 0,
		],
		'view' => [
			'theme' => [
				'pathMap' => [
					'@vendor/amnah/yii2-user/views' => '@frontend/views/user',
				],
			],
		],
	],

	'modules' => [
		'user' => [
			'class' => 'amnah\yii2\user\Module',
			'useUsername' => false,
//			'layout' => '//main2',
			'controllerMap' => [
				'default' => 'common\controllers\users\DefaultController',
				'admin' => 'common\controllers\users\AdminController',
			],
		],
		'rbac' =>  [
			'class' => 'johnitvn\rbacplus\Module',
		],
	],
];
