<?php

namespace common\controllers\users;

use common\models\search\UserSearch;
use Yii;
use common\models\User;
use amnah\yii2\user\models\UserToken;
use amnah\yii2\user\models\UserAuth;
use yii\helpers\Html;
use yii\swiftmailer\Mailer;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * AdminController implements the CRUD actions for User model.
 */
class AdminController extends \amnah\yii2\user\controllers\AdminController
{

    /**
     * List all User models
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var UserSearch $searchModel */
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * Display a single User model
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$model = $this->findModel($id);
//	    var_dump($model->getUserName()); die;
	    $request = Yii::$app->request;
	    if($request->isAjax){
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    return [
			    'title'=> 'Пользователь '.$model->getUserName(),
			    'content'=>$this->renderAjax('view', [
				    'user' => $model,
			    ]),
			    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
				    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
		    ];
	    }

        return $this->render('view', [
            'user' => $model,
        ]);
    }

    /**
     * Create a new User model. If creation is successful, the browser will
     * be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var \amnah\yii2\user\models\User $user */
        /** @var \amnah\yii2\user\models\Profile $profile */

        $user = $this->module->model("User");
        $user->setScenario("admin");
        $profile = $this->module->model("Profile");

        $post = Yii::$app->request->post();
        $userLoaded = $user->load($post);
        $profile->load($post);

        // validate for ajax request
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($user, $profile);
        }

        if ($userLoaded && $user->validate() && $profile->validate()) {
	        $user->save(false);
	        $profile->setUser($user->id)->save(false);

	        $this->sendPEmail($user);

            return $this->redirect(['view', 'id' => $user->id]);
        }

        return $this->render('create', compact('user', 'profile'));
    }

	/**
	 * @param $user
	 * @return bool
	 */
    public function sendPEmail($user){
	    /** @var Mailer $mailer */
	    /** @var \amnah\yii2\user\models\UserToken $userToken */

	    $expireTime = $this->module->resetExpireTime;
	    $expireTime = $expireTime ? gmdate("Y-m-d H:i:s", strtotime($expireTime)) : null;

	    // create userToken
	    $userToken = $this->module->model("UserToken");
	    $userToken = $userToken::generate($user->id, $userToken::TYPE_PASSWORD_RESET, null, $expireTime);
	    $subject = Yii::$app->id . " - Приглашение";

	    // modify view path to module views
	    $mailer = Yii::$app->mailer;
	    $result = $mailer->compose('forgotPassword', compact("subject", "user", "userToken"))
		    ->setTo($user->email)
		    ->setSubject($subject)
		    ->send();

	    return $result;
    }

    /**
     * Update an existing User model. If update is successful, the browser
     * will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // set up user and profile
        $user = $this->findModel($id);
        $user->setScenario("admin");
        $profile = $user->profile;

        $post = Yii::$app->request->post();
        $userLoaded = $user->load($post);
        $profile->load($post);

        // validate for ajax request
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($user, $profile);
        }

        // load post data and validate
        if ($userLoaded && $user->validate() && $profile->validate()) {
            $user->save(false);
            $profile->setUser($user->id)->save(false);
            return $this->redirect(['view', 'id' => $user->id]);
        }

        // render
        return $this->render('update', compact('user', 'profile'));
    }

    /**
     * Delete an existing User model. If deletion is successful, the browser
     * will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // delete profile and userTokens first to handle foreign key constraint
        $user = $this->findModel($id);
        $profile = $user->profile;
        UserToken::deleteAll(['user_id' => $user->id]);
        UserAuth::deleteAll(['user_id' => $user->id]);
        $profile->delete();
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Find the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /** @var \common\models\User $user */
        $user = User::findOne($id);
        if ($user) {
            return $user;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
