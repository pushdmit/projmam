<?php

namespace common\controllers\users;

use common\models\forms\LoginForm;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Default controller for User module
 */
class DefaultController extends \amnah\yii2\user\controllers\DefaultController
{
    /**
     * Display login page
     */
    public function actionLogin()
    {
        /** @var \common\models\forms\LoginForm $model */
        $model = new LoginForm();

        // load post data and login
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->validate()) {
            $returnUrl = $this->performLogin($model->getUser(), $model->rememberMe);
            return $this->redirect($returnUrl);
        }

        return $this->render('login', compact("model"));
    }
}