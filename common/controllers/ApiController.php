<?php

namespace common\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ApiController extends Controller
{
	protected $_status = 200;
	protected $_type = '';
	protected $result = ['status'=>200, 'result'=>'test api'];

	protected $config = [];

	public function behaviors()
	{
		$access = [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['test'],
				'rules' => [
					[
						'actions' => ['test'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'test' => ['post'],
				],
			],
		];

		if(YII_ENV_PROD)
		{
			$access['verbs']['actions']['*'] = ['post'];
		}

		return $access;
	}

	public function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	public function afterAction($action, $result)
	{
		header('Content-Type: application/json');
		echo json_encode($this->result); die;
	}

	public function config($name = null, $default = '')
	{
		if($name){
			if(isset($this->config[$name])){
				return $this->config[$name];
			}else{
				return $default;
			}
		}
		return $this->config;
	}

	public function actionIndex(){
		return $this->result;
	}

	public function get($key = null, $default = null){
		$query = \Yii::$app->request->get()+\Yii::$app->request->post();

		if($key){
			if(key_exists($key, $query)){
				return $query[$key];
			}
			return $default;
		}

		return $query;
	}

	public function post($key = null, $default = null){
		if($key){
			return \Yii::$app->request->post($key, $default);
		}

		//return \Yii::$app->request->get();
		return \Yii::$app->request->post()+\Yii::$app->request->get();
	}

	/**
	 * @param $params string|array
	 * @return array|bool
	 */
	public function check_error($params){
		$errors = [];

		if(is_array($params)){
			$array = $this->get();
			foreach($params as $val){
				if(is_string($val)){
					if(!array_key_exists($val, $array)){
						$errors[$val] = 'Нет '.$val.'.';
					}
				}
			}
		} else {
			if(is_string($params)){
				if(!array_key_exists($params, $this->get())){
					$errors[$params] = 'Нет '.$params.'.';
				}
			}
		}

		if($errors){
			return $errors;
		}

		return false;
	}

}