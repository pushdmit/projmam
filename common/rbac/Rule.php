<?php

namespace common\rbac;

class Rule extends \yii\rbac\Rule
{
	public $name = 'MyRule';

	public function execute($user, $item, $params){
		return \Yii::$app->user->can('admin');
	}
}
