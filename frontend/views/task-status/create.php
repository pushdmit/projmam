<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaskStatus */
/* @var $project common\models\Projects */

$this->title = Yii::t('frontend', 'New task status');
?>

<?= $this->render('/project/_breadcrumbs', ['model' => $model, 'project' => $project, 'name' => 'Manager']) ?>

<?= $this->render('/project/_menu', ['model' => $project]) ?>

<div class="task-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
