<?php

use yii\widgets\ActiveForm;
use common\models\TaskStatus;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'task_status_id')->dropDownList(TaskStatus::getProjectStatus($project->id, true)) ?>

	<input id="task_pks" type="hidden" name="pks">

    <?php ActiveForm::end(); ?>

</div>

<script>
	var selectedIds = [];
	$('input:checkbox[name="selection[]"]').each(function () {
		if (this.checked)
			selectedIds.push($(this).val());
	});
	$('#task_pks').val(selectedIds.join(','));
</script>
