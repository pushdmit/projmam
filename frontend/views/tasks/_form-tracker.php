<?php

use yii\widgets\ActiveForm;
use common\models\TaskTrackers;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $project common\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'task_trackers_id')->dropDownList(TaskTrackers::getForProject($project->id, true)) ?>

	<input id="task_pks" type="hidden" name="pks">

    <?php ActiveForm::end(); ?>

</div>

<script>
	var selectedIds = [];
	$('input:checkbox[name="selection[]"]').each(function () {
		if (this.checked)
			selectedIds.push($(this).val());
	});
	$('#task_pks').val(selectedIds.join(','));
</script>
