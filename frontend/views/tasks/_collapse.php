<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $project common\models\Projects */

$searchChangesModel = new \common\models\search\TaskChangesSearch();
$dataProvider = $searchChangesModel->search(['TaskChangesSearch'=>['task_id'=>$model->id]]);
?>

<div class="row">
<div class="col-md-6">
	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'id',
			'name',
			'description:ntext',
			'project_id',
			'user_name',
			'user_executor_name',
			'task_status_name',
			'is_delete',
		],
	]) ?>
</div>

<div class="col-md-6">
	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'user_id',
				'format' => 'raw',
				'label' => Yii::t('frontend', 'User'),
				'value' => function($data){
					return $data->user_id;
				}
			],
			[
				'attribute' => 'fields',
				'format' => 'raw',
				'label' => Yii::t('frontend', 'Fields'),
				'value' => function($data){
					$result = '';
			        foreach(json_decode($data->fields, true) as $item){
				        $result .= $item['name'];
				        $result .= ' - '.$item['new'];
				        $result .= '<br>';
			        }
					return $result;
				}
			],
		],
	]); ?>
</div>
</div>

