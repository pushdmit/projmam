<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\TaskStatus;
use common\models\TaskTrackers;
use common\helper\StatusHelper;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $project common\models\Projects */
/* @var $form yii\widgets\ActiveForm */
$model->priority = 5;

//$this->registerJs("CKEDITOR.config.codeSnippet_languages = {
//    javascript: 'JavaScript',
//    php: 'PHP'
//};", \yii\web\View::POS_LOAD);
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="col-md-12">
		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	</div>

	<div class="col-md-6">
		<?= $form->field($model, 'task_trackers_id')->dropDownList(TaskTrackers::getForProject($project->id, true)) ?>
		<?= $form->field($model, 'task_status_id')->dropDownList(TaskStatus::getProjectStatus($project->id, true)) ?>
		<?= $form->field($model, 'priority')->dropDownList(StatusHelper::getPriority()) ?>
		<?= $form->field($model, 'user_executor_id')->dropDownList($project->getUserMap()) ?>
	</div>

	<div class="col-md-6">
		<?= $form->field($model, 'parent_task_id')->dropDownList([''=>'Задача']+$project->getProjectTasksMap(true)) ?>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group field-tasks-date_start">
					<label for="tasks-date_start" class="control-label">Date Start</label>
					<?= \yii\jui\DatePicker::widget([
						'id' => 'tasks-date_start',
						'name' => 'Tasks[date_start]',
						'attribute' => 'date_start',
						'options' => [
							'class' => 'form-control',
						],
						'value' => $model->date_start,
						'language' => Yii::$app->language,
						'dateFormat' => 'yyyy-MM-dd',
					]); ?>
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group field-tasks-date_end">
					<label for="tasks-date_end" class="control-label">Date End</label>
					<?= \yii\jui\DatePicker::widget([
						'id' => 'tasks-date_end',
						'name' => 'Tasks[date_end]',
						'options' => [
							'class' => 'form-control',
						],
						'value' => $model->date_end,
						'language' => Yii::$app->language,
						'dateFormat' => 'yyyy-MM-dd',
					]); ?>
					<div class="help-block"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'time_evaluation')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-6">
				<?= $form->field($model, 'readiness')->dropDownList(\common\models\Tasks::$readiness_list) ?>
			</div>
		</div>


	</div>

	<? if(!$model->isNewRecord){ ?>
	<div class="col-md-12">
		<hr>
		<h3>Затраченое время</h3>
		<?= $this->render('/task-time/_form', ['model' => new \common\models\TaskTime(), 'form' => $form]) ?>
		<hr>
	</div>
	<? } ?>

	<div class="col-md-12">
		<?//= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
		<?= $form->field($model, 'description')->widget(CKEditor::className(), [
			'options' => [
				'rows' => 6,
			],
			'preset' => 'basic',
			'clientOptions' => [
//				'toolbarGroups' => [
//					['name' => 'undo'],
//					['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
//					['name' => 'colors'],
//					['name' => 'links', 'groups' => ['links', 'insert']],
//					['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi', 'snippet']],
//					['name' => 'others', 'groups' => ['others', 'about']],
//				],
				'extraPlugins' => 'codesnippet',
			],
		]) ?>
	</div>

	<? if(!Yii::$app->request->isAjax){ ?>
		<div class="col-md-12">
		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>
	    </div>
	<? } ?>

    <?php ActiveForm::end(); ?>

</div>

<script type="application/javascript">
//	CKEDITOR.config.codeSnippet_languages = {
//		javascript: 'JavaScript',
//		php: 'PHP'
//	};
</script>
