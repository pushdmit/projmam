<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $project common\models\Projects */

/* @var $searchCaseModel common\models\search\TaskCaseSearch */
/* @var $dataCaseProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Задача №'.$model->id.' '.$model->name);

?>

<?= $this->render('/project/_breadcrumbs', ['model' => false, 'project' => $project, 'name' => 'Зазачи']) ?>

<div class="tasks-view">

	<? if(!Yii::$app->request->isAjax){ ?>

	    <h1><?= Html::encode($this->title) ?></h1>

	    <p>
	        <?= Html::a(Yii::t('frontend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	        <?= Html::a(Yii::t('frontend', 'Delete'), ['delete', 'id' => $model->id], [
	            'class' => 'btn btn-danger',
	            'data' => [
	                'confirm' => Yii::t('frontend', 'Are you sure you want to delete this item?'),
	                'method' => 'post',
	            ],
	        ]) ?>
	    </p>

	<? } ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'project_id',
            'user_id',
            'user_executor_id',
//            'task_status_id',
//            'is_delete',
        ],
    ]) ?>

	<? if(!Yii::$app->request->isAjax){ ?>
		<?= $this->render('/task-case/index', [
			'task' => $model,
			'dataProvider' => $dataCaseProvider,
			'searchModel' => $searchCaseModel
		]) ?>
	<? } ?>

</div>

<?php Modal::begin([
	"id"=>"ajaxCrudModal",
	"footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
