<?php

use yii\widgets\ActiveForm;
use common\models\User;
use common\models\TaskTrackers;
use common\models\TaskStatus;
use common\models\Tasks;

/* @var $this yii\web\View */
/* @var $model common\models\search\TasksSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $project \common\models\Projects */

$js = <<< JS
	$('[type=reset]').click(function() {
		$('#crud-datatable-filters').find('input:first-child').change();
	});
JS;
$this->registerJs($js, yii\web\View::POS_END);

$def = [''=>'Не выбран']
?>

<div id="crud-datatable-filters">

    <?php $form = ActiveForm::begin([
        'action' => ['/project/'.$project->alias.'/tasks'],
        'method' => 'get',
        'enableClientValidation' => false,
	    'options' => [
		    'data-pjax' => 'true'
	    ],
    ]); ?>

	<div class="row">

		<div class="col-md-2">
			<?= $form->field($model, 'task_trackers_id')->dropDownList($def+TaskTrackers::getForProject($project->id, true)) ?>
		</div>
		<div class="col-md-2">
			<?= $form->field($model, 'user_id')->dropDownList($def+User::getUserMap()) ?>
		</div>

		<div class="col-md-2">
			<?= $form->field($model, 'user_executor_id')->dropDownList($def+User::getUserMap()) ?>
		</div>

		<div class="col-md-2">
			<?= $form->field($model, 'task_status_id')->dropDownList($def+TaskStatus::getProjectStatus($project->id, true)) ?>
		</div>

		<div class="col-md-2">
			<?= $form->field($model, 'readiness')->dropDownList($def+Tasks::$readiness_list) ?>
		</div>
	</div>

    <div class="form-group">
        <?//= Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?//= Html::resetButton(Yii::t('frontend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
