<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use frontend\assets\CrudAsset;
use common\widgets\ButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project common\models\Projects */

CrudAsset::register($this);

$this->title = Yii::t('frontend', 'Задачи');

$script = <<< JS
$(document).ready(function() {
   $('[data-toggle="tooltip"]').tooltip();
});
JS;
$this->registerJs($script);

$script = Yii::$app->request->isAjax?'<script>$(\'[data-toggle="tooltip"]\').tooltip();</script>':'';
?>

<?//= $this->render('/project/_breadcrumbs', ['model' => false, 'project' => $project,]) ?>

<?= $this->render('/project/_menu', ['model' => $project]) ?>

<div class="tasks-index">
	<?= $this->render('_search', ['model' => $searchModel, 'project' => $project]); ?>

	<?= GridView::widget([
		'id'=>'crud-datatable',
		'dataProvider' => $dataProvider,
//		'filterModel' => $searchModel,
		'containerOptions'=>['style'=>'overflow: auto'],
		'pjax'=>true,
		'showPageSummary'=>true,
		'toolbar'=> [
			['content'=>
				Html::a(
					'<i class="glyphicon glyphicon-plus"></i>',
					['/project/'.$project->alias.'/tasks/create'],
					['role'=>'modal-remote','title'=> 'Create new Task Times','class'=>'btn btn-default']
				).
				Html::a(
					'<i class="glyphicon glyphicon-repeat"></i>',
					['/project/'.$project->alias.'/tasks'],
					['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset']
				),
			],
		],
		'striped' => true,
		'condensed' => true,
		'responsive' => true,
		'panel' => [
			'heading' => '<i class="glyphicon glyphicon-list"></i> Задачи',
			'after'=> ButtonWidget::widget([
				'buttons'=>Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Статус',
					['/project/'.$project->alias.'/tasks/bulk-status'] ,
					[
						"class"=>"btn btn-primary btn-xs",
						'role'=>'modal-remote-bulk',
						'data-confirm'=>false, 'data-method'=>false,
//						'data-request-method'=>'post',
//						'data-confirm-title'=>'Are you sure?',
//						'data-confirm-message'=>'Are you sure want to delete this item'
					]
				).' '.Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Готовность',
					['/project/'.$project->alias.'/tasks/bulk-readiness'] ,
					[
						"class"=>"btn btn-primary btn-xs",
						'role'=>'modal-remote-bulk',
						'data-confirm'=>false,
						'data-method'=>false,
					]
				).' '.Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Трекер',
					['/project/'.$project->alias.'/tasks/bulk-tracker'] ,
					[
						"class"=>"btn btn-primary btn-xs",
						'role'=>'modal-remote-bulk',
						'data-confirm'=>false,
						'data-method'=>false,
					]
				),
			]).$script.'<div class="clearfix"></div>',
		],
		'columns' => require(__DIR__.'/_columns.php'),
	]); ?>
</div>

<?php Modal::begin([
	"id"=>"ajaxCrudModal",
//	'options' => [
//		'tabindex' => 1,
//	],
	'size' => Modal::SIZE_LARGE,
	"footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
