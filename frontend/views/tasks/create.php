<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $project common\models\Projects */
$this->title = Yii::t('frontend', 'New Tasks');
?>

<? if(!Yii::$app->request->isAjax){ ?>

	<?= $this->render('/project/_breadcrumbs', ['model' => $model, 'project' => $project, 'name' => 'Tasks']) ?>

	<h1><?= Html::encode($this->title) ?></h1>

<? } ?>

<div class="tasks-create">

    <?= $this->render('_form', [
        'model' => $model,
        'project' => $project,
    ]) ?>

</div>

<div class="clearfix"></div>
