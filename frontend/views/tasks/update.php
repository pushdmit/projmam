<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $project common\models\Projects */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Tasks',
]) . $model->name;
?>

<? if(!Yii::$app->request->isAjax){ ?>

	<?= $this->render('/project/_breadcrumbs', ['model' => $model, 'project' => $project, 'name' => 'Редактирование']) ?>

	<h1><?= Html::encode($this->title) ?></h1>

<? } ?>

<div class="tasks-update">

    <?= $this->render('_form', [
        'model' => $model,
	    'project' => $project,
    ]) ?>

</div>

<div class="clearfix"></div>
