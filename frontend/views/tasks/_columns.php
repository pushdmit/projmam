<?php

use common\helper\StatusHelper;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Projects;
use common\models\TaskStatus;
use common\models\Tasks;

/* @var $project common\models\Projects */

$columns = [
	['class' => '\kartik\grid\CheckboxColumn'],
	[
		'class'=>'kartik\grid\ExpandRowColumn',
		'width'=>'50px',
		'value'=>function ($model, $key, $index, $column) {
			return GridView::ROW_COLLAPSED;
		},
		'detail'=>function ($model, $key, $index, $column) {
			return Yii::$app->controller->renderPartial('/tasks/_collapse', [
				'model'=>$model,
				'project'=>Projects::getProject()
			]);
		},
		'headerOptions'=>['class'=>'kartik-sheet-style'],
		'expandOneOnly'=>true
	],
	[
		'class'=>'kartik\grid\EditableColumn',
		'attribute' => 'priority',
		'format' => 'raw',
		'value' => function($data){
			return StatusHelper::getPriorityName($data->priority);
		},
		'filterType'=> GridView::FILTER_SELECT2,
		'filter' => StatusHelper::getPriority(),
		'filterWidgetOptions'=>[
			'pluginOptions'=>['allowClear'=>true],
		],
		'filterInputOptions'=>['placeholder'=>'Приоритет'],
//				'group'=>true,
		'refreshGrid'=>true,
		'editableOptions'=>[
			'header'=>Yii::t('frontend', 'Приоритет'),
			'size'=>'md',
			'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
			'widgetClass'=> '\kartik\select2\Select2',
			'ajaxSettings' => [
				'url' => '/api/tasks/update'
			],
			'options'=>[
				'hideSearch'=>true,
				'data'=>[
					StatusHelper::getPriority()
				],
				'pluginOptions'=>[
					'autoclose'=>true
				]
			]
		],
	],
	[
		'class'=>'kartik\grid\EditableColumn',
		'attribute' => 'task_status_id',
		'format' => 'raw',
		'value' => function($data){
			return $data->task_status_name;
		},
		'filterType'=> GridView::FILTER_SELECT2,
		'filter' => TaskStatus::getProjectStatus($project->id, true),
		'filterWidgetOptions'=>[
			'pluginOptions'=>['allowClear'=>true],
		],
		'filterInputOptions'=>['placeholder' => Yii::t('frontend', 'Статус')],
		'refreshGrid' => true,
		'editableOptions'=>[
			'header'=>Yii::t('frontend', 'Статус'),
			'size'=>'md',
			'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
			'widgetClass'=> '\kartik\select2\Select2',
			'ajaxSettings' => [
				'url' => '/api/tasks/update'
			],
			'options'=>[
				'hideSearch'=>true,
				'data'=>[
					TaskStatus::getProjectStatus($project->id, true)
				],
				'pluginOptions'=>[
					'autoclose'=>true
				]
			]
		],
	],
	[
		'class'=>'kartik\grid\EditableColumn',
		'attribute' => 'readiness',
		'format' => 'raw',
		'headerOptions' => ['width' => '140px'],
		'value' => function($data){
			return '<div class="progress" style="width: 110px; margin-bottom: 2px;">
					  <div class="progress-bar" role="progressbar" aria-valuenow="'.$data->readiness.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$data->readiness.'%;">
					    '.$data->readiness.'%
					  </div>
					</div>';
		},
		'filterType'=> GridView::FILTER_SELECT2,
		'filter' => Tasks::$readiness_list,
		'filterWidgetOptions'=>[
			'pluginOptions'=>['allowClear'=>true],
		],
		'filterInputOptions'=>['placeholder'=>'Готовность'],
		'refreshGrid' => true,
		'editableOptions'=>[
			'header'=>Yii::t('frontend', 'Готовность'),
			'size'=>'md',
			'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
			'widgetClass'=> '\kartik\select2\Select2',
			'ajaxSettings' => [
				'url' => '/api/tasks/update'
			],
			'options'=>[
				'hideSearch'=>true,
				'data'=>[
					Tasks::$readiness_list
				],
				'pluginOptions'=>[
					'autoclose'=>true
				]
			]
		],
	],
	[
		'attribute' => 'name',
		'format' => 'raw',
		'value' => function($data) use ($project) {
			$name = $data->parent_task_id?$data->parent_task_id.'-':'';
			$name = '#'.$name.$data->id.': '.$data->name;
			return Html::a($name, '/project/'.$project->alias.'/tasks/view?id='.$data->id, ['data-pjax'=>'0']);
		}
	],
	[
		'attribute' => 'user_executor_id',
		'format' => 'raw',
		'value' => function($data){
			return $data->user_executor_name;
		}
	],
	[
		'attribute' => 'time_evaluation',
		'format' => 'raw',
		'contentOptions'=>['style'=>'text-align:right'],
		'value' => function($data){
			return $data->time_evaluation;
		},
		'pageSummary' => true,
		'pageSummaryFunc' => GridView::F_SUM,
		'pageSummaryOptions'=>['style'=>'text-align:right;'],
	],
	[
		'attribute' => 'time_elapsed',
		'format' => 'raw',
		'contentOptions'=>['style'=>'text-align:right'],
		'value' => function($data){
			return $data->time_elapsed;
		},
		'pageSummary' => true,
		'pageSummaryFunc' => GridView::F_SUM,
		'pageSummaryOptions'=>['style'=>'text-align:right;'],
	],
	[
		'class' => '\kartik\grid\ActionColumn',
		'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>'],
		'buttons' => [
			'time' => function ($url, $data) {
				return Html::a(
					'<i class="glyphicon glyphicon-plus" style="color:green" data-toggle="tooltip" data-placement="top" title="Затраченое время"></i>'
					, Url::toRoute(['/task-time/create', 'TaskTime[task_id]' => $data->id])
					, ['role' => 'modal-remote']
				);
			},
			'case' => function ($url, $data) {
				return Html::a(
					'<i class="glyphicon glyphicon-th-list" style="color:green" data-toggle="tooltip" data-placement="top" title="Дела"></i>'
					, Url::toRoute(['/task-case/index', 'TaskCaseSearch[task_id]' => $data->id])
					, ['role' => 'modal-remote']
				);
			},
		],
		'updateOptions'=>['role'=>'modal-remote','title'=>'Редактировать', 'data-toggle'=>'tooltip'],
		'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр', 'data-toggle'=>'tooltip'],
		'urlCreator' => function($action, $model){
			return ['/project/'.Projects::getAlias().'/tasks/'.$action.'?id='.$model->id];
		},
		'template' => '{time} {case} / {view} {update} {delete}',
	],
];
return $columns;