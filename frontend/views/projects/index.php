<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helper\StatusHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('frontend', 'Create Projects'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
	
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

	        'user_id',
	        [
		        'attribute' => 'name',
		        'format' => 'raw',
		        'label' => Yii::t('frontend', 'Name'),
		        'value' => function($data){
			        return Html::a($data->name, '/project/'.$data->alias);
		        }
	        ],
	        [
		        'attribute' => 'status',
		        'format' => 'raw',
		        'label' => Yii::t('frontend', 'Status'),
		        'value' => function($data){
//			        $option = [];
			        return '<span class="'.StatusHelper::getStatusClass($data->status).'">'.StatusHelper::getStatusName($data->status).'</span>';
		        }
	        ],
	        [
		        'attribute' => 'is_active',
		        'format' => 'raw',
		        'label' => Yii::t('frontend', 'Активирован'),
		        'value' => function($data){
//			        $option = [];
			        return '<span class="'.StatusHelper::getStatusClass($data->is_active).'">'.StatusHelper::getIsName($data->is_active).'</span>';
		        }
	        ],
	        [
		        'attribute' => 'is_public',
		        'format' => 'raw',
		        'label' => Yii::t('frontend', 'Публичный'),
		        'value' => function($data){
//			        $option = [];
			        return '<span class="'.StatusHelper::getStatusClass($data->is_public).'">'.StatusHelper::getIsName($data->is_public).'</span>';
		        }
	        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
