<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\widgets\ButtonWidget;

/* @var $this yii\web\View */
/* @var $task \common\models\Tasks */
/* @var $searchModel common\models\search\TaskCaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = Yii::t('app', 'Задачи');
//$this->params['breadcrumbs'][] = $this->title;

\frontend\assets\CrudAsset::register($this);

?>
<div class="task-case-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable-case',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
	        'pjaxSettings' => [
	        	'push'=>false, 'replace'=>false, 'pushRedirect'=>false, 'replaceRedirect'=>false
	        ],
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['/task-case/create', 'TaskCase[task_id]'=>$task->id],
                    ['role'=>'modal-remote','title'=> 'Create new Task Cases','class'=>'btn btn-default'])
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'before'=>'',
	            'heading' => '<i class="glyphicon glyphicon-list"></i> Дела',
                'after'=>ButtonWidget::widget([
                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Закрыть',
                        ["/task-case/bulk-closed"] ,
                        [
                            "class"=>"btn btn-danger btn-xs",
                            'role'=>'modal-remote-bulk',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Are you sure?',
                            'data-confirm-message'=>'Are you sure want to delete this item'
                        ]),
                ]).
                '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
