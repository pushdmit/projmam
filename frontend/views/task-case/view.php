<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TaskCase */
?>
<div class="task-case-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'task_id',
            'number',
            'is_closed',
            'name',
        ],
    ]) ?>

</div>
