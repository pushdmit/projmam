<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaskCase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-case-form">

    <?php $form = ActiveForm::begin(); ?>

	<? if($model->task_id){ ?>
		<?= $form->field($model, 'task_id', ['options'=> ['class' => 'form-group-hide'], 'template'=>'{input}'])->hiddenInput(); ?>
	<? } else { ?>
		<?= $form->field($model, 'task_id')->textInput() ?>
	<? } ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
