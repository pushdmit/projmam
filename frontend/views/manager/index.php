<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */
?>

<?= $this->render('/project/_breadcrumbs', ['model' => false, 'project' => $model, 'name' => 'Manager']) ?>

<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('/project/_menu', ['model' => $model]) ?>
	
</div>
