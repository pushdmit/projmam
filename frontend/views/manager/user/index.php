<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProjectUsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project common\models\Projects */

$this->title = Yii::t('frontend', 'Users');
?>

<?= $this->render('/project/_breadcrumbs', ['model' => false, 'project' => $project, 'name' => 'Manager']) ?>

<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('/project/_menu', ['model' => $project]) ?>

	<?php Pjax::begin(); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'project_id',
			'user_id',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
	<?php Pjax::end(); ?></div>
	
</div>
