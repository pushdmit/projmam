<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectUsers */
/* @var $project common\models\Projects */
?>

<?= $this->render('/project/_breadcrumbs', ['model' => false, 'project' => $project, 'name' => 'Manager']) ?>

<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('/project/_menu', ['model' => $project]) ?>

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'user_id')->dropDownList(\common\models\User::getUserMap()) ?>

	<div class="col-md-12">
		<div class="form-group">
			<?= Html::submitButton(Yii::t('frontend', 'Add'), ['class' => 'btn btn-success']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
	
</div>
