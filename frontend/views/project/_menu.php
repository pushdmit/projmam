<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */
?>

<div class="btn-group" role="group" aria-label="..." style="padding:0 0 30px">
    <?= Html::a(Yii::t('frontend', 'Новая задача'), ['/project/'.$model->alias.'/tasks/create'], [
    	'class' => 'btn btn-default', 'role' => 'modal-remote'
    ]) ?>
    <?= Html::a(Yii::t('frontend', 'Задачи'), ['/project/'.$model->alias.'/tasks'], ['class' => 'btn btn-default']) ?>
    <?= Html::a(Yii::t('frontend', 'Диаграммы Ганта'), ['/project/'.$model->alias.'/gantt'], ['class' => 'btn btn-default']) ?>
    <?= Html::a(Yii::t('frontend', 'Календарь'), ['/project/'.$model->alias.'/calendar'], ['class' => 'btn btn-default']) ?>
    <?= Html::a(Yii::t('frontend', 'Wiki'), ['/project/'.$model->alias.'/wiki'], ['class' => 'btn btn-default']) ?>
	<div class="btn-group" role="group">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?=Yii::t('frontend', 'Управление')?>
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li>
				<?= Html::a(Yii::t('frontend', 'Manager'), ['/project/'.$model->alias.'/manager']) ?>
			</li>
			<li>
				<?= Html::a(Yii::t('frontend', 'Task Status Create'), ['/project/'.$model->alias.'/manager/task-status-create']) ?>
			</li>
			<li>
				<?= Html::a(Yii::t('frontend', 'Add user'), ['/project/'.$model->alias.'/manager/user-add']) ?>
			</li>
			<li>
				<?= Html::a(
					Yii::t('finance', 'Новый трекер'),
					['/project/'.$model->alias.'/task-trackers/create'],
					['role' => 'modal-remote']
				) ?>
			</li>
		</ul>
	</div>
</div>
