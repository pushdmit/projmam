<?php
/* @var $project common\models\Projects */
/* @var $name string */

if(!isset($name)){
	$name = 'Tasks';
}

list($module, $controller, $action) = explode('/', Yii::$app->controller->route);

$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Projects'), 'url' => ['/projects']];
$this->params['breadcrumbs'][] = ['label' => $project->name, 'url' => ['/project/'.$project->alias]];
if('index' != $action)
	$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', $name), 'url' => ['/project/'.$project->alias.'/'.$controller]];
$this->params['breadcrumbs'][] = $this->title;
?>
