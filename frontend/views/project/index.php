<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Projects */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Projects'), 'url' => ['/projects']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_menu', ['model' => $model]) ?>

	<div class="row">
		<div class="col-md-4">
			<?= \common\widgets\TimeWidget::widget() ?>
		</div>
	</div>
	
</div>
