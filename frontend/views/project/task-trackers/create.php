<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaskTrackers */

?>
<div class="task-trackers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
