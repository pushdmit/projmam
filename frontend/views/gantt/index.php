<?php

use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project common\models\Projects */
/* @var $tasks common\models\Tasks[] */
/* @var $links string */

\frontend\assets\CrudAsset::register($this);

$this->title = Yii::t('frontend', 'Диаграммы Ганта');
?>

<script src="/js/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/dhtmlxgantt_marker.js" type="text/javascript" charset="utf-8"></script>
<script src="/js/locale/locale_ru.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="/css/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

<?= $this->render('/project/_breadcrumbs', ['model' => false, 'project' => $project,]) ?>

<?= $this->render('/project/_menu', ['model' => $project]) ?>

<div class="tasks-index">

	<? Pjax::begin(['id'=>'crud-datatable-pjax']); ?>
	<div id="gantt_here" style="height:480px;"></div>

	<? foreach($tasks as $item){ ?>
		<a id="task-<?=$item->id?>" href="/project/<?=$project->alias?>/tasks/update?id=<?=$item->id?>" role="modal-remote"></a>
	<? } ?>

	<script type="text/javascript">
		var tasks = {
			data:<?=json_encode($data)?>,
			links:<?=json_encode($links)?>
		};

		gantt.config.sort = false;

		gantt.config.scale_unit = "month";
		gantt.config.date_scale = "%F, %Y";
		gantt.config.scale_height = 50;
		gantt.config.subscales = [
			{unit:"day", step:1, date:"%j, %D" }
		];

		gantt.config.columns = [
			{name:"text", label:"Название", tree:true, width:'*', resize:true },
			{name:"start_date", label:"Дата начала", align: "center", resize:true },
//			{name:"duration", label:"Прод.", align: "center" }
		];

		gantt.config.details_on_dblclick = false;
		gantt.config.drag_links = false;
		gantt.config.drag_move = false;
		gantt.config.drag_progress = false;
		gantt.config.drag_resize = false;

		gantt.config.row_height = 26;

		gantt.attachEvent("onTaskDblClick", function(id, e) {
			$('#task-'+id).click();
		});

//		gantt.config.work_time = true;
//		gantt.config.correct_work_time = true;

		gantt.init("gantt_here");
		gantt.clearAll();

		var todayMarker = gantt.addMarker({ start_date:new Date(), css:"today", text:"",  title:""});
		setInterval(function(){
			var today = gantt.getMarker(todayMarker);
			today.start_date = new Date();
			today.title = "";
			gantt.updateMarker(todayMarker);
		}, 1000*60);
		gantt.parse(tasks);
	</script>

	<? Pjax::end(); ?>
</div>

<?php Modal::begin([
	"id"=>"ajaxCrudModal",
	'size' => Modal::SIZE_LARGE,
	"footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
