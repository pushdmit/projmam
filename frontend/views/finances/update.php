<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Finance */
?>
<div class="finance-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
