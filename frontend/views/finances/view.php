<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Finance */
?>
<div class="finance-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'user_id',
        ],
    ]) ?>

</div>
