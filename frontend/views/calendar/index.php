<?php

use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $project common\models\Projects */
/* @var $tasks common\models\Tasks[] */
/* @var $links string */

\frontend\assets\CrudAsset::register($this);
\frontend\assets\CalendarAsset::register($this);

$script = <<< JS
	addCalendar();
JS;
$this->registerJs($script);

$this->title = Yii::t('frontend', 'Календарь');
?>

<!--<script src="/js/calendar/bootstrap-year-calendar.js" type="text/javascript" charset="utf-8"></script>-->
<!--<script src="/js/calendar/bootstrap-year-calendar.ru.js" type="text/javascript" charset="utf-8"></script>-->
<link rel="stylesheet" href="/js/calendar/bootstrap-year-calendar.css" type="text/css" media="screen" title="no title" charset="utf-8">

<?= $this->render('/project/_breadcrumbs', ['model' => false, 'project' => $project,]) ?>

<?= $this->render('/project/_menu', ['model' => $project]) ?>

<div class="tasks-index">

	<? Pjax::begin(['id'=>'crud-datatable-pjax']); ?>
	<div id="calendar"></div>

	<div class="clearfix"></div>

	<a id="task-create" date-href="/project/<?=$project->alias?>/tasks/create" data-modal-size="large"
	   href="/project/<?=$project->alias?>/tasks/create" role="modal-remote"></a>
	<? foreach($tasks as $item){ ?>
		<a id="task-<?=$item->id?>" href="/project/<?=$project->alias?>/tasks/update?id=<?=$item->id?>"
		   role="modal-remote" data-modal-size="large"></a>
		<a id="task-d-<?=$item->id?>" class="crud-datatable-action-del"
		   data-confirm-title="Are you sure?" data-confirm-message="Are you sure want to delete this item"
		   data-pjax="false" data-pjax-container="crud-datatable-pjax" data-request-method="post"
		   href="/project/<?=$project->alias?>/tasks/delete?id=<?=$item->id?>" role="modal-remote-bulk2"></a>
	<? } ?>

	<script type="text/javascript">
		function editEvent(event) {
			var a = $('#task-create');

			if(undefined!=event.id){
				a = $('#task-'+event.id);
			} else {
				var href = a.attr('date-href');
				var startDate = event.startDate.getFullYear()+'-'+(event.startDate.getMonth()+1)+'-'+event.startDate.getDate();
				var endDate = event.endDate.getFullYear()+'-'+(event.endDate.getMonth()+1)+'-'+event.endDate.getDate();
				href = href+"?Tasks[date_start]="+startDate+"&Tasks[date_end]="+endDate;
				a.attr('href', href);
			}

			a.click();
		}

		function deleteEvent(event) {
			var a = $('#task-create');
			if(undefined!=event.id){
				a = $('#task-d-'+event.id);
				console.log(a);
				a.click();
			}
		}

		function saveEvent() {

		}

		function addCalendar() {
			$(function() {
				var currentYear = new Date().getFullYear();
				var currentDate = new Date();

				$('#calendar').calendar({
					enableContextMenu: true,
					enableRangeSelection: true,
					language: 'ru',
					contextMenuItems:[
						{
							text: 'Редактировать',
							click: editEvent
						}
						, {
							text: 'Удалить',
							click: deleteEvent
						}
					],
					selectRange: function(e) {
						editEvent({ startDate: e.startDate, endDate: e.endDate });
					},
					mouseOnDay: function(e) {
						if(e.events.length > 0) {
							var content = '';

							for(var i in e.events) {
								content += '<div class="event-tooltip-content">'
									+ '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
									+ '<div class="event-location">' + e.events[i].location + '</div>'
									+ '</div>';
							}

							$(e.element).popover({
								trigger: 'manual',
								container: 'body',
								html:true,
								content: content
							});

							$(e.element).popover('show');
						}
					},
					mouseOutDay: function(e) {
						if(e.events.length > 0) {
							$(e.element).popover('hide');
						}
					},
					dayContextMenu: function(e) {
						$(e.element).popover('hide');
					},
					dataSource: [
						<? foreach($data as $item){ ?>
						{
							id: '<?=$item['id']?>',
							name: '<?=$item['name']?>',
							location: '<?=$item['location']?>',
							startDate: new Date(<?=$item['startDate']?>),
							endDate: new Date(<?=$item['endDate']?>)
						},
						<? } ?>
					],
					customDayRenderer: function(element, date) {
						if(date.getYear() == currentDate.getYear()
							&& date.getMonth() == currentDate.getMonth()
							&& date.getDate() == currentDate.getDate())
						{
//							$(element).css('font-weight', 'bold');
//							$(element).css('font-size', '15px');
//							$(element).css('color', 'green');

							$(element).css('background-color', 'red');
							$(element).css('color', 'white');
							$(element).css('border-radius', '15px');

//							$(element).css('border', '2px solid blue');
						}
					}
				});

				$('#save-event').click(function() {
					saveEvent();
				});
			});
		}

		<? if(Yii::$app->request->isAjax){ ?>
			addCalendar();
		<? } ?>
	</script>

	<? Pjax::end(); ?>
</div>

<?php Modal::begin([
	"id"=>"ajaxCrudModal",
	'size' => Modal::SIZE_LARGE,
	"footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
