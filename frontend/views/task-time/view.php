<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TaskTime */
?>
<div class="task-time-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'task_id',
            'user_id',
            'quantity',
            'date_create',
        ],
    ]) ?>

</div>
