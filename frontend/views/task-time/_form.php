<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaskTime */
/* @var $form yii\widgets\ActiveForm */

$is_form = true;
if(isset($form)){
	$is_form = false;
}
?>

<div class="task-time-form">

    <? if($is_form){ ?>
        <?php $form = ActiveForm::begin(); ?>
	<? } else {
	    $form->enableClientValidation = false;
    } ?>

    <?//= $form->field($model, 'task_id')->textInput() ?>

    <?//= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>

    <?//= $form->field($model, 'date_create')->textInput() ?>

  
	<?php if($is_form){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
		<?php ActiveForm::end(); ?>
	<? } else {
		$form->enableClientValidation = true;
	} ?>

</div>
