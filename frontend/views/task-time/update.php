<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TaskTime */
?>
<div class="task-time-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
