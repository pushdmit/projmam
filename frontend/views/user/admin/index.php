<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var amnah\yii2\user\Module $module
 * @var amnah\yii2\user\models\search\UserSearch $searchModel
 * @var amnah\yii2\user\models\User $user
 * @var amnah\yii2\user\models\Role $role
 */

\frontend\assets\CrudAsset::register($this);

$module = $this->context->module;
$user = $module->model("User");
$role = $module->model("Role");

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('user', 'Create {modelClass}', [
          'modelClass' => 'User',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?//\yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
	    'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
	    'pjax' => true,
	    'rowOptions'=> function(\common\models\User $model){
    	    $option = [];
		    if($model->isOnline()){
			    $option['class'] = 'success';
		    }
		    return $option;
	    },
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id' => [
	            'attribute' => 'id',
	            'contentOptions' => ['style' => 'width:8px;'],
	            'value' => function(\common\models\User $model) use ($user) {
	            	if($model->logged_in_at>date('Y-m-d', time()-60*10)){
			            return $model->id;
		            }
		            return $model->id;
	            },
            ],
	        'first_name',
	        'last_name',
//	        'middle_name',
            [
                'attribute' => 'status',
                'label' => Yii::t('user', 'Status'),
                'filter' => $user::statusDropdown(),
                'value' => function($model, $index, $dataColumn) use ($user) {
                    $statusDropdown = $user::statusDropdown();
                    return $statusDropdown[$model->status];
                },
            ],
            'email:email',
	        [
		        'label' => 'Roles',
		        'content' => function($model) {
			        $authManager = Yii::$app->authManager;
			        $idField = Yii::$app->getModule('rbac')->userModelIdField;
			        $roles = [];
			        foreach ($authManager->getRolesByUser($model->{$idField}) as $role) {
				        $roles[] = $role->name;
			        }
			        if(count($roles)==0){
				        return Yii::t("yii","(not set)");
			        }else{
				        return implode(",", $roles);
			        }
		        }
	        ],
            [
            	'class' => 'kartik\grid\ActionColumn',
	            'contentOptions' => ['style' => 'width: 120px;'],
	            'width' => '98px',
	            'header' => Yii::t('rbac', 'Actions'),
	            'dropdown' => false,
	            'vAlign' => 'middle',
	            'buttons' => [
		            'assignment' => function($action, \common\models\User $model) {
			            $url = \yii\helpers\Url::to(['/rbac/assignment/assignment', 'id' => $model->id]);
			            return Html::a('<span class="glyphicon glyphicon-cog"></span>', $url, [
				            'title' => \Yii::t('yii', 'Assignment'),
				            'role' => 'modal-remote',
				            'data-toggle' => 'tooltip',
			            ]);
		            },
	            ],
	            'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
	            'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
	            'deleteOptions' => [
		            'role' => 'modal-remote'
		            , 'title' => 'Delete',
		            'data-confirm' => false
		            , 'data-method' => false,
		            'data-request-method' => 'post',
		            'data-toggle' => 'tooltip',
		            'data-confirm-title' => Yii::t('app', 'Are you sure?'),
		            'data-confirm-message' => Yii::t('app', 'Are you sure want to delete this item')
	            ],
	            'template' => '{assignment} / {view}{update}{delete}',
            ],
        ],
    ]); ?>
    <?//\yii\widgets\Pjax::end(); ?>

</div>

<?php \yii\bootstrap\Modal::begin([
	"id" => "ajaxCrudModal",
	"footer" => "",
]) ?>
<?php \yii\bootstrap\Modal::end(); ?>
