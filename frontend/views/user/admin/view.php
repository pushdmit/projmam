<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\User $user
 */

$this->title = 'Пользователь '.$user->getUserName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

	<? if(!Yii::$app->request->isAjax){ ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('yii', 'Update'), ['update', 'id' => $user->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('yii', 'Delete'), ['delete', 'id' => $user->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('user', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<? } ?>

    <?= DetailView::widget([
        'model' => $user,
        'attributes' => [
            'id',
	        'first_name',
	        'last_name',
	        'middle_name',
            'email:email',
	        'status' => [
	        	'attribute' => 'status',
		        'value' => $user::statusDropdown()[$user->status],
	        ],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
