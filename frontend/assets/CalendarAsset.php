<?php 

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author John Martin <john.itvn@gmail.com>
 * @since 1.0
 */
class CalendarAsset extends AssetBundle
{
    public $sourcePath = '@ajaxcrud/assets';

    public $css = [
        'ajaxcrud.css'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\CrudAsset',
    ];
    
   public function init() {
       $this->js = YII_DEBUG ? [
           '/js/calendar/bootstrap-year-calendar.js',
       ]:[
           '/js/calendar/bootstrap-year-calendar.js',
       ];

	   $this->js[] = '/js/calendar/bootstrap-year-calendar.ru.js';

       parent::init();
   }
}
