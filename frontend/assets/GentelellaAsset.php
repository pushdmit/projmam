<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GentelellaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
	
    public $css = [
        'templates/gentelella/css/bootstrap.min.css',
        'templates/gentelella/fonts/css/font-awesome.min.css',
        'templates/gentelella/css/animate.min.css',
        'templates/gentelella/css/custom.css',
        'templates/gentelella/css/maps/jquery-jvectormap-2.0.3.css',
        'templates/gentelella/css/icheck/flat/green.css',
        'templates/gentelella/css/floatexamples.css',
//        'css/site.css',
    ];
	
    public $js = [
	    'templates/gentelella/js/jquery.min.js',
	    'templates/gentelella/js/bootstrap.min.js',
	    'templates/gentelella/js/progressbar/bootstrap-progressbar.min.js',
	    'templates/gentelella/js/icheck/icheck.min.js',
	    'templates/gentelella/js/moment/moment.min.js',
	    'templates/gentelella/js/datepicker/daterangepicker.js',
	    'templates/gentelella/js/chartjs/chart.min.js',
	    'templates/gentelella/js/sparkline/jquery.sparkline.min.js',
	    'templates/gentelella/js/custom.js',
	    'templates/gentelella/js/flot/jquery.flot.js',
	    'templates/gentelella/js/flot/jquery.flot.pie.js',
	    'templates/gentelella/js/flot/jquery.flot.orderBars.js',
	    'templates/gentelella/js/flot/jquery.flot.time.min.js',
	    'templates/gentelella/js/flot/date.js',
	    'templates/gentelella/js/flot/jquery.flot.spline.js',
	    'templates/gentelella/js/flot/jquery.flot.stack.js',
	    'templates/gentelella/js/flot/curvedLines.js',
	    'templates/gentelella/js/flot/jquery.flot.resize.js',
	    'templates/gentelella/js/pace/pace.min.js',
    ];
}
