<?php 

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author John Martin <john.itvn@gmail.com>
 * @since 1.0
 */
class KCFinderAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/base.css',
	];
    
   public function init() {
	   $this->js[] = '/js/base.js';
       parent::init();
   }
}
