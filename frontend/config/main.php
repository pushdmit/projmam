<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'ProjMan',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'languagepicker', 'translatemanager'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'rules' => [
				'<module:(project)>/<element_id>/wiki' => 'wiki/article/index',
				'<module:(project)>/<element_id>/wiki/<controler>' => 'wiki/<controler>/index',
				'<module:(project)>/<element_id>/wiki/<controler>/<action>' => 'wiki/<controler>/<action>',

	            '<module:(project)>/<alias>' => 'project/index',
	            '<module:(project)>/<alias>/<controler>' => 'project/<controler>/index',
	            '<module:(project)>/<alias>/<controler>/<action>' => 'project/<controler>/<action>',

				'<module:(finance)>/<id>' => '<module>/transaction/index',
				'<module:(finance)>/<id>/<controler>' => '<module>/<controler>/index',
				'<module:(finance)>/<id>/<controler>/<action>' => '<module>/<controler>/<action>',
            ],
        ],
		'languagepicker' => [
			'class' => 'lajax\languagepicker\Component',        // List of available languages (icons and text)
			'languages' => [
				'ru' => 'Русский',
				'en' => 'English',
				'fr' => 'Français',
			],
		]
    ],
    'modules' => [
	    'finance' => [
		    'class' => 'common\modules\finance\Module',
	    ],
		'wiki' => [
			'class' => 'common\modules\wiki\Module',
			'defaultRoute' => 'article',
			'languages' => [
				'ru' => 'Русский',
				'en' => 'English',
				'fr' => 'Français',
			],
		],
        'permit' => [
            'class' => 'developeruz\db_rbac\Yii2DbRbac',
//            'layout' => '//admin'
            'params' => [
                'userClass' => 'common\models\User'
            ]
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'allowedIPs' => ['*']
        ],
	    'gridview' =>  [
		    'class' => '\kartik\grid\Module'
		    // 'downloadAction' => 'gridview/export/download',
		    // 'i18n' => []
	    ],
	    'datecontrol' =>  [
		    'class' => 'kartik\datecontrol\Module',
		    'displaySettings' => [
			    \kartik\datecontrol\Module::FORMAT_DATE => 'dd-MM-yyyy',
			    \kartik\datecontrol\Module::FORMAT_TIME => 'HH:mm:ss a',
			    \kartik\datecontrol\Module::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss a',
		    ],
		    'saveSettings' => [
			    \kartik\datecontrol\Module::FORMAT_DATE => 'php:U',
			    \kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
			    \kartik\datecontrol\Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
		    ],
		    'displayTimezone' => 'Asia/Kolkata',
		    'saveTimezone' => 'UTC',
		    'autoWidget' => true,
		    'ajaxConversion' => true,
		    'autoWidgetSettings' => [
			    \kartik\datecontrol\Module::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoclose'=>true]], // example
			    \kartik\datecontrol\Module::FORMAT_DATETIME => [],
			    \kartik\datecontrol\Module::FORMAT_TIME => [],
		    ],
		    'widgetSettings' => [
			    \kartik\datecontrol\Module::FORMAT_DATE => [
				    'class' => 'yii\jui\DatePicker', // example
				    'options' => [
					    'dateFormat' => 'php:d-M-Y',
					    'options' => ['class'=>'form-control'],
				    ]
			    ]
		    ]
	    ],
    ],
    'params' => $params,
];
