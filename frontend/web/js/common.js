$(document).ready(function () {
    $('#financetransaction-type_id').change(function(){
        var element = $('.div-hidden');
        var type = $(this).val();

        element.css('display', 'none');

        element.each(function(el){
            if(el.data('view-type').offset(type)+1){
                el.show();
            }
        });
    });
});