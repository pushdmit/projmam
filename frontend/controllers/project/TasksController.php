<?php

namespace frontend\controllers\project;

use common\models\Projects;
use common\models\search\TaskCaseSearch;
use common\models\TaskTime;
use Yii;
use common\models\Tasks;
use common\models\search\TasksSearch;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TasksController implements the CRUD actions for Tasks model.
 */
class TasksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TasksSearch();
	    
	    $project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
	    $project->setStatic();
	    $searchModel->project_id = $project->id;
	    
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

//		var_dump(Yii::$app->languagepicker->languages); die;

        return $this->render('/tasks/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project' => $project,
        ]);
    }

    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	    $model = $this->findModel($id);
	    $project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));

	    $params = [
		    'model' => $model,
		    'project' => $project,
	    ];

	    $request = Yii::$app->request;
	    if($request->isAjax){
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    return [
			    'title'=> "TaskTime #".$id,
			    'content'=>$this->renderAjax('/tasks/view', $params),
			    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
				    Html::a(
				    	'Edit',['/project/'.$project->alias.'/tasks/update','id'=>$id],
					    ['class'=>'btn btn-primary',
					    'role'=>'modal-remote']
				    )
		    ];
	    } else {
		    $searchCaseModel = new TaskCaseSearch();
		    $searchCaseModel->task_id = $model->id;
		    $dataCaseProvider = $searchCaseModel->search(Yii::$app->request->queryParams);

		    $params += [
			    'searchCaseModel' => $searchCaseModel,
			    'dataCaseProvider' => $dataCaseProvider,
		    ];

		    return $this->render('/tasks/view', $params);
	    }
    }

    /**
     * Creates a new Tasks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate()
	{
		$model = new Tasks();
		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));

		$request = Yii::$app->request;

		if($model->isNewRecord){
			$model->load($request->get());
			$model->project_id = $project->id;
			$model->user_id = Yii::$app->user->id;

			if(!$model->date_start){
				$model->date_start = date('Y-m-d');
			}

			if(!$model->date_end){
				$model->date_end = date('Y-m-d');
			}
		}

		$params = [
			'model' => $model,
			'project' => $project,
		];

		if($request->isAjax){
			Yii::$app->response->format = Response::FORMAT_JSON;
			if($request->isGet){
				return [
					'title'=> "Создание задачи",
					'size'=> "large",
					'content'=>$this->renderAjax('/tasks/create', $params),
					'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

				];
			}else if($model->load($request->post()) && $model->save()){
				return [
					'forceReload'=>'#crud-datatable-pjax',
					'title'=> "Create new TaskTime",
					'content'=>'<span class="text-success">Задача создана</span>',
					'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::a('Create More',['project/'.$project->alias.'/tasks/create']
							,['class'=>'btn btn-primary','role'=>'modal-remote'])
				];
			}else{
				return [
					'title'=> "Создание задачи",
					'content'=>$this->renderAjax('/tasks/create', $params),
					'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

				];
			}
		}else{
			if ($model->load($request->post()) && $model->save()) {
				return $this->redirect(['project/'.$project->alias.'/tasks']);
			} else {
				return $this->render('/tasks/create', $params);
			}
		}
	}

    /**
     * Updates an existing Tasks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	    $project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));

	    $request = Yii::$app->request;

	    $params = [
		    'model' => $model,
		    'project' => $project,
	    ];

	    if($request->isAjax){
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    if($request->isGet){
			    return [
				    'title'=> "Редактирование #".$id,
				    'size'=> "large",
				    'content'=>$this->renderAjax('/tasks/update', $params),
				    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
					    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
			    ];
		    }else if($model->load($request->post()) && $model->save()){

			    if(!$model->isNewRecord){
				    $model->addTaskTime();
			    }

			    return [
				    'forceReload'=>'#crud-datatable-pjax',
				    'title'=> "Задача #".$id,
				    'content'=>$this->renderAjax('/tasks/view', $params),
				    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
					    Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
			    ];
		    }else{
			    return [
				    'title'=> "Редактирование #".$id,
				    'content'=>$this->renderAjax('/tasks/update', $params),
				    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
					    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
			    ];
		    }
	    }else{
		    if ($model->load($request->post()) && $model->save()) {
			    return $this->redirect(['project/'.$project->alias.'/tasks']);
		    } else {
			    return $this->render('/tasks/update', $params);
		    }
	    }
    }

    /**
     * Deletes an existing Tasks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    $request = Yii::$app->request;
	    $project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
        $this->findModel($id)->delete();

	    if($request->isAjax){
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
	    }else{
		    return $this->redirect(['project/'.$project->alias.'/tasks']);
	    }
    }

	public function actionBulkStatus()
	{
		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
		$request = Yii::$app->request;

		$params = [
			'project' => $project,
			'model' => new Tasks(),
		];

		if($request->isAjax){
			$result = false;

			if($request->post('pks')){
				$count = 0;
				$pks = explode(',', $request->post('pks'));
				foreach($pks as $pk) {
					$model = $this->findModel($pk);
					$model->load($request->post());
					if($model->save()) $count++;
				}

				if(count($pks)==$count) $result = true;
			}

			Yii::$app->response->format = Response::FORMAT_JSON;

			if($request->isGet){
				return [
					'title'=> "Редактирование",
					'content'=>$this->renderAjax('/tasks/_form-status', $params),
					'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
				];
			}else if($result){
				return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
			}
		}

		if($request->isAjax){
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
		} else {
			return $this->redirect(['project/'.$project->alias.'/tasks']);
		}
	}

	public function actionBulkReadiness()
	{
		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
		$request = Yii::$app->request;

		$params = [
			'project' => $project,
			'model' => new Tasks(),
		];

		if($request->isAjax){
			$result = false;

			if($request->post('pks')){
				$count = 0;
				$pks = explode(',', $request->post('pks'));
				foreach($pks as $pk) {
					$model = $this->findModel($pk);
					$model->load($request->post());
					if($model->save()) $count++;
				}

				if(count($pks)==$count) $result = true;
			}

			Yii::$app->response->format = Response::FORMAT_JSON;

			if($request->isGet){
				return [
					'title'=> "Редактирование",
					'content'=>$this->renderAjax('/tasks/_form-readiness', $params),
					'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
				];
			}else if($result){
				return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
			}
		}

		if($request->isAjax){
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
		} else {
			return $this->redirect(['project/'.$project->alias.'/tasks']);
		}
	}

	public function actionBulkTracker()
	{
		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
		$request = Yii::$app->request;

		$params = [
			'project' => $project,
			'model' => new Tasks(),
		];

		if($request->isAjax){
			$result = false;

			if($request->post('pks')){
				$count = 0;
				$pks = explode(',', $request->post('pks'));
				foreach($pks as $pk) {
					$model = $this->findModel($pk);
					$model->load($request->post());
					if($model->save()) $count++;
				}

				if(count($pks)==$count) $result = true;
			}

			Yii::$app->response->format = Response::FORMAT_JSON;

			if($request->isGet){
				return [
					'title'=> "Редактирование",
					'content'=>$this->renderAjax('/tasks/_form-tracker', $params),
					'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
						Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
				];
			}else if($result){
				return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
			}
		}

		if($request->isAjax){
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
		} else {
			return $this->redirect(['project/'.$project->alias.'/tasks']);
		}
	}

    /**
     * Finds the Tasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
