<?php

namespace frontend\controllers\project;

use common\models\Projects;
use common\models\ProjectUsers;
use common\models\search\ProjectUsersSearch;
use common\models\TaskStatus;
use Yii;
use yii\web\Controller;

class ManagerController extends Controller
{
//	public $layout = 'gentelella';

	public function actionIndex()
	{
		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));

//		var_dump(Yii::$app->controller->route); die;

		return $this->render('/manager/index', [
			'model' => $project,
		]);
	}

	public function actionTaskStatusCreate()
	{
		$model = new TaskStatus();
		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
		
		$model->project_id = $project->id;

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['/project/'.$project->alias.'/manager']);
		} else {
			return $this->render('/task-status/create', [
				'model' => $model,
				'project' => $project,
			]);
		}
	}

	public function actionUserAdd()
	{
		$model = new ProjectUsers();
		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));

		$model->project_id = $project->id;

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['/project/'.$project->alias.'/manager/user']);
		} else {
			return $this->render('/manager/user/add', [
				'model' => $model,
				'project' => $project,
			]);
		}
	}

	public function actionUser()
	{
		$searchModel = new ProjectUsersSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));

		return $this->render('/manager/user/index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'project' => $project,
		]);
	}
}
