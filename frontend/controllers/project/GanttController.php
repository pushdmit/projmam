<?php

namespace frontend\controllers\project;

use common\components\behaviors\AccessBehavior;
use common\helper\DateHelper;
use common\models\search\TasksSearch;
use common\models\Tasks;
use Yii;
use common\models\Projects;
use frontend\models\ProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class GanttController extends Controller
{
    /**
     * Lists all Projects models.
     * @return mixed
     */
	public function actionIndex()
	{
		$searchModel = new TasksSearch();

		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
		$project->setStatic();
		$searchModel->project_id = $project->id;

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$query = $dataProvider->query;
		$tasks = $query->all();
		$data = [];
		$links = [];

		/** @var Tasks $item */
		foreach($tasks as $item){
//			var_dump(DateHelper::getDiff($item->date_start, $item->date_end)); die;
			$data[] = [
				'id' => $item->id,
				'text' => $item->name,
				'start_date' => Yii::$app->formatter->asDate($item->date_start, 'dd-MM-Y'),
				'duration' => DateHelper::getDiff($item->date_start, $item->date_end)+1,
				'order' => 1,
				'parent' => $item->parent_task_id,
				'progress' => $item->readiness*0.01,
				'open' => true,
			];
			if($item->parent_task_id){
				$links[] = [
					'id' => count($links)+1,
					'source' => $item->parent_task_id,
					'target' => $item->id,
					'type' => 1,
				];
			}
		}

		return $this->render('/gantt/index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'project' => $project,
			'tasks' => $tasks,
			'data' => $data,
			'links' => $links,
		]);
	}
}
