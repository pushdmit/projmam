<?php

namespace frontend\controllers\project;

use common\components\behaviors\AccessBehavior;
use common\helper\DateHelper;
use common\models\search\TasksSearch;
use common\models\Tasks;
use Yii;
use common\models\Projects;
use frontend\models\ProjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class CalendarController extends Controller
{
    /**
     * Lists all Projects models.
     * @return mixed
     */
	public function actionIndex()
	{
		$searchModel = new TasksSearch();

		$project = Projects::findAliasOrId(Yii::$app->request->getQueryParam('alias'));
		$project->setStatic();
		$searchModel->project_id = $project->id;

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$query = $dataProvider->query;
		$tasks = $query->all();
		$data = [];
		$links = [];

		/** @var Tasks $item */
		foreach($tasks as $item){
			$dataStart = explode('-', $item->date_start);
			$dataStart[1] = $dataStart[1]-1;
			$dataStart = implode(',', $dataStart);

			$endDate = explode('-', $item->date_end);
			$endDate[1] = $endDate[1]-1;
			$endDate = implode(',', $endDate);

//			var_dump($dataStart); die;
			$data[] = [
				'id' => $item->id,
				'name' => $item->id.'# '.$item->name,
				'location' => '', //$item->description,
				'startDate' => $dataStart,
				'endDate' => $endDate,
			];
			if($item->parent_task_id){
				$links[] = [
					'id' => count($links)+1,
					'source' => $item->parent_task_id,
					'target' => $item->id,
					'type' => 1,
				];
			}
		}

		return $this->render('/calendar/index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'project' => $project,
			'tasks' => $tasks,
			'data' => $data,
			'links' => $links,
		]);
	}
}
