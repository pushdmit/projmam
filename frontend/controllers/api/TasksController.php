<?php
/**
 * Created by PhpStorm.
 * User: �������
 * Date: 15.10.2015 9:44
 */

namespace frontend\controllers\api;

use common\models\Tasks;
use Yii;
use common\controllers\ApiController;
use yii\web\NotFoundHttpException;

class TasksController extends ApiController
{

	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actionIndex(){
		//$this->enableCsrfValidation = false;

		if($error = $this->check_error(['term'])){
			$this->result = ['status'=>600, 'errors'=>$error];
			return false;
		}

		$models = [];

		$categories = Shops::searchCategories($this->get('term'));

		if (!$this->get('request')){
			if($categories){
				array_push($models, ['name'=>'Категории', 'alias'=>'', 'image'=>'', 'type'=>'hide', 'label' => $this->get('term')]);
				foreach($categories as $item){
					$item['label'] = $this->get('term');
					array_push($models, $item+['type'=>'category']);
				}
				array_push($models, ['type'=>'hr']);
			}
		}

		$search = Shops::search($this->get('term'), 10-count($categories), false);

		if($search["hits"]){
			if (!$this->get('request')) {
				array_push($models, ['name' => 'Магазины', 'alias' => '', 'image' => '', 'type' => 'hide', 'label' => $this->get('term')]);
			}
			foreach($search["hits"] as $item){
//				print_r($item);die;
				$item['cashback'] = NumberHelper::NumberFormat($item['cashback'], Currency::getCurrencySymbol($item['payment_units']));
//				$item['cashback'] = NumberHelper::NumberFormat(Settings::getPaymentSizeUser($item['cashback'], $item['cashback']), Currency::getCurrencySymbol($item['payment_units']));
				$item['label'] = $this->get('term'); //$item['name'];
				array_push($models, $item);
			}
		}

		if($models)
		{
			return $this->result = ['status'=>200, 'result'=>$models, 'meta'=>$search['meta']];
		}

		return $this->result = ['status'=>404, 'errors'=>['core'=>'Товаров не найдено']];
	}

	public function actionUpdate()
	{
		$params = $this->get();
//		var_dump($params['Tasks'][$this->get('editableKey')]); die;

		$model = $this->findModel($this->get('editableKey'));

		if ($model->load($params['Tasks'][$this->get('editableIndex')], '') && $model->save()) {
			return $this->result = ['output'=>'Решена', 'message'=>''];
		}

		return $this->result = ['status'=>404, 'errors'=>['core'=>'Товаров не найдено']];
	}

	/**
	 * @param $id
	 * @return Tasks
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id)
	{
		if (($model = Tasks::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}