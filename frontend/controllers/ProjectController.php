<?php

namespace frontend\controllers;

use common\components\behaviors\AccessBehavior;
use common\models\TaskStatus;
use common\models\Tasks;
use Yii;
use common\models\Projects;
use yii\web\Controller;
use yii\filters\VerbFilter;

class ProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'as AccessBehavior' => [
                'class' => AccessBehavior::className(),
	            'rules' =>
		            ['project' =>
			            [
				            [
					            'actions' => ['index', 'view'],
					            'allow' => true,
					            'roles' => ['@'],
				            ],
			            ]
		            ]
            ]
        ];
    }
	
    public function actionIndex($alias)
    {
        $project = Projects::find()->where(['alias' => $alias])->one();

//		var_dump(Yii::t('frontend', 'Проекты')); die;

        return $this->render('index', [
	        'model' => $project,
        ]);
    }



	/**
	 * Creates a new Tasks model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionTaskCreate()
	{
		$model = new Tasks();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$project = Projects::findOne($model->project_id);
			return $this->redirect(['/project/'.$project->alias]);
		} else {
			return $this->render('/tasks/create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Creates a new Tasks model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionTaskStatusCreate()
	{
		$model = new TaskStatus();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$project = Projects::findOne($model->project_id);
			return $this->redirect(['/project/'.$project->alias]);
		} else {
			return $this->render('/task-status/create', [
				'model' => $model,
			]);
		}
	}
}
