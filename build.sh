#!/bin/sh -> bash

DIRSCRIPT=$(pwd); # Текущая директория
echo "${DIRSCRIPT}";

for arg in "$@"; do
	case $arg in
		-composer|-c)
			composer update
		;;
		-sphinx|-s)
			if [ -e "/usr/share/sphinxsearch" ]
			then
				echo "Sphinxsearch установлен";
			else
				echo "Установка sphinxsearch";
				cd ~/
				apt-get update
#				apt-get upgrade
				apt-get install libodbc1
				apt-get install libpq5
#				wget http://sphinxsearch.com/files/sphinxsearch_2.2.10-release-1~jessie_i386.deb
				wget http://sphinxsearch.com/files/sphinxsearch_2.2.10-release-1~jessie_amd64.deb
				dpkg --install ~/sphinxsearch_2.2.10-release-1~jessie_amd64.deb
				cd ${DIRSCRIPT}
				service sphinxsearch start
			fi;

			cat "${DIRSCRIPT}/src/sphinxsearch/sphinx_db.conf" > "${DIRSCRIPT}/src/sphinxsearch/sphinx.conf"
			cat "${DIRSCRIPT}/src/sphinxsearch/sphinx_index.conf" >> "${DIRSCRIPT}/src/sphinxsearch/sphinx.conf"

			cp "${DIRSCRIPT}/src/sphinxsearch/sphinx.conf" "/etc/sphinxsearch/sphinx.conf"
			cp "${DIRSCRIPT}/src/sphinxsearch/wordforms.txt" "/etc/sphinxsearch/wordforms.txt"
#			service sphinxsearch stop
			indexer --all  --rotate
#			service sphinxsearch start
		;;
		-translate|-t)
			php yii translate/scan;
			# php yii translate/optimize;
		;;
		-migrate|-m)
			php yii migrate --migrationPath="@vendor/amnah/yii2-user/migrations";
			php yii migrate --migrationPath="${DIRSCRIPT}/vendor/lajax/yii2-translate-manager/migrations";
			php yii migrate --migrationPath="@yii/rbac/migrations/"
			php yii migrate;
#			php yii migrate --migrationPath="${DIRSCRIPT}/common/modules/user/migrations";
			php yii db/refresh
		;;
		-migrate-down|-md)
			php yii migrate/down;
			php yii db/refresh
		;;
		-mkdir)
#			mkdir -p ${DIRSCRIPT}/frontend/web/upload/;
		;;
	esac
done

#chown -R www-data:www-data frontend/web/images/
#chown -R www-data:www-data backend/runtime/tmp/
# chmod -R 0777 frontend/web/images/C

